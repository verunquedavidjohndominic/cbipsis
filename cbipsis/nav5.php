<?php session_start();
error_reporting(1);
date_default_timezone_set('Asia/Manila');
include("connection.php");
$date = ucfirst(date('F j, Y'));
$page_validator = $_SESSION['valid'];
$class_checker = mysqli_query($con, "SELECT * FROM login WHERE id = '$page_validator'") or die("error logging in");
$row = mysqli_fetch_assoc($class_checker);
$class = $row['class'];
$location = $row['location'];
$branch = $row['branch'];
$bn_list = mysqli_query($con, "SELECT * FROM branch");
$bn_no = mysqli_query($con, "SELECT * FROM branch_no");
$getid = $_GET['id'];
$getloc = $_GET['loc'];
$getbranch = $_GET['branch'];
$box_name_fetch = mysqli_query($con, "SELECT * FROM products WHERE id = '$getid'");
$box_name_row = mysqli_fetch_assoc($box_name_fetch);
$box_name = strtoupper($box_name_row['product_name']);
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<?php
if (isset($_POST['info1'])) {
    $info_id = $_POST['id'];
    ?>
        <meta http-equiv="refresh" content="0;url=dashboard.php?dash=nav17&id=<?php echo $info_id?>" />
    <?php
}
if (isset($_POST['info'])) {
    $info_id = $_POST['id'];
    ?>
        <meta http-equiv="refresh" content="0;url=dashboard.php?dash=nav18&id=<?php echo $info_id?>" />
    <?php
}
if(isset($_SESSION['valid']))
    {
        ////////important per page
     
        ////////
        ?>
        <style type="text/css"> #nav5,#nav-5-1 {color: white; font-weight: bold; background-color:gray;} #nav-5-1{padding: 8px; border-radius: 2px; position: static;} #nav5:hover,#nav-5-1:hover { color: white; background-color: black; text-shadow: none;}</style>
        <?php
     	if ($getloc=="") {
     		$header_info = "";
     	} else {
     		$header_info = strtoupper($getloc);
     	}
		?>
		<div class="row">
		<div class="col-sm"></div>
		<div class="col-sm-12">
		<div class="row">
		<div class="col-sm-6">
		    <!------------------------------------------------->
		    <div class="card">
                    <div class="card-body">
                        <form name="search" method="post">
                            <header style="position: absolute; font-weight: bold; color: gray; padding-top: 2px;">PRODUCT LIST</header>
                            <div style="text-align: right;">
                                <input type="text" name="search" placeholder="Search" style="text-align: center; border-radius: 5em; border: 1px solid gray;">
                                <button type="submit" name="searchbnt_user" style="background-color: white; border-radius: 5cm; width: 25px; border: 1px solid gray;"><i class="fa fa-search"></i></button>
                            </div>
                        </form>
                        <br>
                        <br>
                        <?php
                        //////////////////////////////////////////////////////////////
                        $products = mysqli_query($con, "SELECT * FROM products WHERE product_name LIKE '%$search%' ORDER BY product_name");
                        $products_result_num_row = mysqli_num_rows($products);
                        if ($products_result_num_row<=0) {
                            ?>
                            <div class="table-responsive" style="max-height: 400px;">
                                <table class="table table-responsive-sm-6 mb-0">
                                    <thead>
                                        <tr style="text-align: center;">
                                            <th><strong>Name</strong></th>
                                            <th><strong>Description</strong></th>
                                            <th><strong>Price</strong></th>
                                        </tr>
                                    </thead>
                                    <?php
                          
                                
                                ?>
                                </table>
                                <br>
                                No Data
                            </div>
                            <?php
                        }
                        ?>
                        <?php
                        //////////////////////////////////////////////////////////////
                        ?>
                        <div id = "tago">
                        <div class="table-responsive" style="max-height: 400px;">
                            <table class="table table-responsive-sm-6 mb-0">
                                <thead>
                                    <tr style="text-align: center;">
                                        <th><strong>Name</strong></th>
                                        <th><strong>Description</strong></th>
                                        <th><strong>Price</strong></th>
                                    </tr>
                                </thead>
                                <?php
                                    $products = mysqli_query($con, "SELECT * FROM products ORDER BY product_name");
                                    $products_result_num_row = mysqli_num_rows($products);
                                    if ($products_result_num_row>0) {
                                        while ($products_row = mysqli_fetch_assoc($products)) {
                                            $id = $products_row['id'];
                                            $product_name = $products_row['product_name'];
                                            $description = $products_row['description'];
                                            $price = $products_row['price'];

                                            $category_fetch = mysqli_query($con, "SELECT * FROM category WHERE id = '$category'");
                                            $category_fetch_row = mysqli_fetch_assoc($category_fetch);
                                            $category_name = $category_fetch_row['category_name'];


                                                ?>
                                                <form id="" method="POST">
                                                    <input type="hidden" name="id" value="<?php echo $id?>">
                                                    <tr style="text-align: center;">
                                                        <td><?php echo ucfirst($product_name);?></td>
                                                        <td><?php echo ucfirst($description);?></td>
                                                        <td><?php echo ucfirst($price);?></td>
                                                        <td><div class="form-group"><button type="submit" id="submit" name="info1" style="border: none;"><i class="fa fa-info-circle" style="color: black; background-color: white; padding-top: 15px; font-size: 20px"></i></button></div></td>
                                                    </tr>
                                                </form>
                                                <?php
                                        }
                                    } else {
                                        ?>
                                            <tr>
                                                <td>
                                                    <style type="text/css">#tago{display: none;}</style>
                                                    <p style="color:gray;"><?php echo $search?> is not exist</p>
                                                </td>
                                            </tr>
                                        <?php
                                    }
                            
                            ?>
                            </table>
                        </div>
                        </div>
                        <?php
                        if (isset($_POST['search'])) {
                            $search = $_POST['search'];
                        ?>
                            <style type="text/css">#tago{display: none;}</style>
                            <div class="table-responsive" style="max-height: 400px;">
                            <table class="table table-responsive-sm-6 mb-0">
                                <thead>
                                    <tr style="text-align: center;">
                                        <th><strong>Name</strong></th>
                                        <th><strong>Description</strong></th>
                                        <th><strong>Price</strong></th>
                                    </tr>
                                </thead>
                                <?php
                                    $products = mysqli_query($con, "SELECT * FROM products WHERE product_name LIKE '%$search%' ORDER BY product_name");
                                    $products_result_num_row = mysqli_num_rows($products);
                                    if ($products_result_num_row>0) {
                                        while ($products_row = mysqli_fetch_assoc($products)) {
                                            $id = $products_row['id'];
                                            $product_name = $products_row['product_name'];
                                            $description = $products_row['description'];
                                            $price = $products_row['price'];

                                            $category_fetch = mysqli_query($con, "SELECT * FROM category WHERE id = '$category'");
                                            $category_fetch_row = mysqli_fetch_assoc($category_fetch);
                                            $category_name = $category_fetch_row['category_name'];


                                                ?>
                                                <form id="" method="POST">
                                                    <input type="hidden" name="id" value="<?php echo $id?>">
                                                    <tr style="text-align: center;">
                                                        <td><?php echo ucfirst($product_name);?></td>
                                                        <td><?php echo ucfirst($description);?></td>
                                                        <td><?php echo ucfirst($price);?></td>
                                                        <td><div class="form-group"><button type="submit" id="submit" name="info1" style="border: none;"><i class="fa fa-info-circle" style="color: black; background-color: white; padding-top: 15px; font-size: 20px"></i></button></div></td>
                                                    </tr>
                                                </form>
                                                <?php
                                        }
                                    } else {
                                        ?>
                                            <tr>
                                                <td>
                                                    <style type="text/css">#tago{display: none;}</style>
                                                    <p style="color:gray;"><?php echo $search?> No Data</p>
                                                </td>
                                            </tr>
                                        <?php
                                    }
                            
                            ?>
                            </table>
                        </div>
                        <?php
                        }
                        ?>
                        <!---->
                    </div>
                </div>
		    <!------------------------------------------------->
		</div>
		<div class="col-sm-6">
        	<!------------------------------------------------->
        	<div class="card">
                    <div class="card-body">
                        <form name="search" method="post">
                            <header style="position: absolute; font-weight: bold; color: gray; padding-top: 2px;">EQUIPMENT LIST</header>
                            <div style="text-align: right;">
                                <input type="text" name="search1" placeholder="Search" style="text-align: center; border-radius: 5em; border: 1px solid gray;">
                                <button type="submit" name="searchbnt_user" style="background-color: white; border-radius: 5cm; width: 25px; border: 1px solid gray;"><i class="fa fa-search"></i></button>
                            </div>
                        </form>
                        <br>
                        <br>
                        <?php
                        //////////////////////////////////////////////////////////////
                        $products = mysqli_query($con, "SELECT * FROM equipments ORDER BY equip_name");
                        $products_result_num_row = mysqli_num_rows($products);
                        if ($products_result_num_row<=0) {
                            ?>
                            <div class="table-responsive" style="max-height: 400px;">
                                <table class="table table-responsive-sm-6 mb-0">
                                    <thead>
                                        <tr style="text-align: center;">
                                            <th><strong>Name</strong></th>
                                            <th><strong>Description</strong></th>
                                            <th><strong>Price</strong></th>
                                        </tr>
                                    </thead>
                                    <?php
                          
                                
                                ?>
                                </table>
                                <br>
                                No Data
                            </div>
                            <?php
                        }
                        ?>
                        <?php
                        //////////////////////////////////////////////////////////////
                        ?>
                        <div id = "tago1">
                        <div class="table-responsive" style="max-height: 400px;">
                            <table class="table table-responsive-sm mb-0">
                                <thead>
                                    <tr style="text-align: center;">
                                        <th><strong>Name</strong></th>
                                        <th><strong>Description</strong></th>
                                        <th><strong>Price</strong></th>
                                    </tr>
                                </thead>
                                <?php
                                    $products = mysqli_query($con, "SELECT * FROM equipments ORDER BY equip_name");
                                    $products_result_num_row = mysqli_num_rows($products);
                                    if ($products_result_num_row>0) {
                                        while ($products_row = mysqli_fetch_assoc($products)) {
                                            $id = $products_row['id'];
                                            $product_name = $products_row['equip_name'];
                                            $description = $products_row['description'];
                                            $price = $products_row['price'];

                                            $category_fetch = mysqli_query($con, "SELECT * FROM category WHERE id = '$category'");
                                            $category_fetch_row = mysqli_fetch_assoc($category_fetch);
                                            $category_name = $category_fetch_row['category_name'];


                                                ?>
                                                <form id="" method="POST">
                                                    <input type="hidden" name="id" value="<?php echo $id?>">
                                                    <tr style="text-align: center;">
                                                        <td><?php echo ucfirst($product_name);?></td>
                                                        <td><?php echo ucfirst($description);?></td>
                                                        <td><?php echo ucfirst($price);?></td>
                                                        <td><div class="form-group"><button type="submit" id="submit" name="info" style="border: none;"><i class="fa fa-info-circle" style="color: black; background-color: white; padding-top: 15px; font-size: 20px"></i></button></div></td>
                                                    </tr>
                                                </form>
                                                <?php
                                        }
                                    } else {
                                        ?>
                                            <tr>
                                                <td>
                                                    <style type="text/css">#tago1{display: none;}</style>
                                                    <p style="color:gray;"><?php echo $search?> is not exist</p>
                                                </td>
                                            </tr>
                                        <?php
                                    }
                            
                            ?>
                            </table>
                        </div>
                        </div>
                        <?php
                        if (isset($_POST['search1'])) {
                            $search = $_POST['search1'];
                        ?>
                            <style type="text/css">#tago1{display: none;}</style>
                            <div class="table-responsive" style="max-height: 400px;">
                            <table class="table table-responsive-sm mb-0">
                                <thead>
                                    <tr style="text-align: center;">
                                        <th><strong>Name</strong></th>
                                        <th><strong>Description</strong></th>
                                        <th><strong>Price</strong></th>
                                    </tr>
                                </thead>
                                <?php
                                    $products = mysqli_query($con, "SELECT * FROM equipments WHERE equip_name LIKE '%$search%' ORDER BY equip_name");
                                    $products_result_num_row = mysqli_num_rows($products);
                                    if ($products_result_num_row>0) {
                                        while ($products_row = mysqli_fetch_assoc($products)) {
                                            $id = $products_row['id'];
                                            $product_name = $products_row['equip_name'];
                                            $description = $products_row['description'];
                                            $price = $products_row['price'];

                                            $category_fetch = mysqli_query($con, "SELECT * FROM category WHERE id = '$category'");
                                            $category_fetch_row = mysqli_fetch_assoc($category_fetch);
                                            $category_name = $category_fetch_row['category_name'];


                                                ?>
                                                <form id="" method="POST">
                                                    <input type="hidden" name="id" value="<?php echo $id?>">
                                                    <tr style="text-align: center;">
                                                        <td><?php echo ucfirst($product_name);?></td>
                                                        <td><?php echo ucfirst($description);?></td>
                                                        <td><?php echo ucfirst($price);?></td>
                                                        <td><div class="form-group"><button type="submit" id="submit" name="info" style="border: none;"><i class="fa fa-info-circle" style="color: black; background-color: white; padding-top: 15px; font-size: 20px"></i></button></div></td>
                                                    </tr>
                                                </form>
                                                <?php
                                        }
                                    } else {
                                        ?>
                                            <tr>
                                                <td>
                                                    <style type="text/css">#tago1{display: none;}</style>
                                                    <p style="color:gray;"><?php echo $search?> is not exist</p>
                                                </td>
                                            </tr>
                                        <?php
                                    }
                            
                            ?>
                            </table>
                        </div>
                        <?php
                        }
                        ?>
                        <!---->
                    </div>
                </div>
        	<!------------------------------------------------->
		</div>
		<div class="col-sm"></div>
		</div>
		</div>
		<div class="col-sm"></div>
		</div>
		<?php
    }
else
    {
        header("location: index.php");
    }
?>
</body>
</html>