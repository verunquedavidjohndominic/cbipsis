<?php session_start();
error_reporting(1);
date_default_timezone_set('Asia/Manila');
include("connection.php");
$date = ucfirst(date('Y-m-d'));

$page_validator = $_SESSION['valid'];
$class_checker = mysqli_query($con, "SELECT * FROM login WHERE id = '$page_validator'") or die("error logging in");
$row = mysqli_fetch_assoc($class_checker);
$class = $row['class'];
$location = $row['location'];
$branch = $row['branch'];

?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<?php


if(isset($_SESSION['valid']))
    {
        ////////important per page
        if ($class==0) {
           ?>
                <meta http-equiv="refresh" content="0;url=dashboard.php?dash=nav5" />
           <?php
        }
        ////////
        ?>
        <style type="text/css"> #nav3,#nav-3-1 {color: white; font-weight: bold; background-color:gray;} #nav-3-1{padding: 8px; border-radius: 2px; position: static;} #nav3:hover,#nav-3-1:hover { color: white; background-color: black; text-shadow: none;}</style>
        <?php
if (isset($_POST['rec_prod'])) {
    $item_id = $_POST['item_id'];
    $quantity = $_POST['quantity'];
    mysqli_query($con, "INSERT INTO `product_stock`(`quantity`, `date`, `location`, `branch`, `item_id`, `employee`) VALUES ('$quantity','$date','$location','$branch','$item_id','$page_validator')");
    echo "<script>alert('product stock successfuly Receive')</script>";
    ?>
       <meta http-equiv="refresh" content="0;url=dashboard.php?dash=nav3"/>
    <?php
}
if (isset($_POST['rec_eqp'])) {
    $item_id = $_POST['item_id'];
    $quantity = $_POST['quantity'];
    mysqli_query($con, "INSERT INTO `item_stock`(`quantity`, `date`, `location`, `branch`, `item_id`, `employee`) VALUES ('$quantity','$date','$location','$branch','$item_id','$page_validator')");
    echo "<script>alert('product stock successfuly Receive')</script>";
    ?>
       <meta http-equiv="refresh" content="0;url=dashboard.php?dash=nav3"/>
    <?php
}
$product_list = mysqli_query($con, "SELECT * FROM products");
$item_list = mysqli_query($con, "SELECT * FROM equipments");
?>
<div class="row">
    <div class="col-sm"></div>
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm">
                <div class="card">
                    <div class="card-body">
                        <header style="position: absolute; font-weight: bold; color: gray; padding-top: 2px;">RECEIVE PRODUCTS</header>
                        <br>
                        <br>
                        <form id="" method="post">
                            <div class="form-group">
                                <label class="mb-1"><strong>Select Product</strong></label>
                                <select name = "item_id" class="form-control">
                                <?php 
                                   while($product_list_row = mysqli_fetch_array($product_list)):;
                                ?>
                                    <option value="<?php echo $product_list_row[0];?>">
                                    <?php echo ucfirst($product_list_row[1]);?>
                                    </option>
                                <?php 
                                    endwhile;
                                ?>
                                </select>
                            </div>
                            <label class="mb-1"><strong>Quantity</strong></label>
                            <input type="number" class="form-control" name="quantity" placeholder="Enter Quantity" required="" style="text-align: center;">
                            <br>
                            <div class="form-group">
                                <button type="submit" id="submit" name="rec_prod" class="btn btn-primary btn-block" style="background-color: #787312; border-color: #a1990b; box-shadow: 3px 3px 8px #b1b1b1, -3px -3px 8px #ffffff; width: 150px; margin: auto; ">Confirm</button>
                            </div>
                        </form>
                        <!---->
                    </div>
                </div>
            </div>
            <br>
            <div class="col-sm">
                <div class="card">
                    <div class="card-body">
                        <header style="position: absolute; font-weight: bold; color: gray; padding-top: 2px;">RECEIVE EQUIPMENTS</header>
                        <br>
                        <br>
                        <form id="" method="post">
                            <div class="form-group">
                                <label class="mb-1"><strong>Select Equipment</strong></label>
                                <select name = "item_id" class="form-control">
                                <?php 
                                   while($item_list_row = mysqli_fetch_array($item_list)):;
                                ?>
                                    <option value="<?php echo $item_list_row[0];?>">
                                    <?php echo ucfirst($item_list_row[1]);?>
                                    </option>
                                <?php 
                                    endwhile;
                                ?>
                                </select>
                            </div>
                            <label class="mb-1"><strong>Quantity</strong></label>
                            <input type="number" class="form-control" name="quantity" placeholder="Enter Quantity" required="" style="text-align: center;">
                            <br>
                            <div class="form-group">
                                <button type="submit" id="submit" name="rec_eqp" class="btn btn-primary btn-block" style="background-color: #787312; border-color: #a1990b; box-shadow: 3px 3px 8px #b1b1b1, -3px -3px 8px #ffffff; width: 150px; margin: auto; ">Confirm</button>
                            </div>
                        </form>
                        <!---->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm">
        <div class="row">
            <div class="col-sm">
                <br>
            </div>
            <div class="col-sm">
                <br>
            </div>
        </div>
    </div>
    <div class="col-sm"></div>
</div>
<?php
    }
else
    {
        header("location: index.php");
    }
?>
</body>
</html>