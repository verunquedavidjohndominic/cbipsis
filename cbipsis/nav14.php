<?php session_start();
error_reporting(1);
date_default_timezone_set('Asia/Manila');
include("connection.php");
$date = ucfirst(date('F j, Y h:i:s'));

$page_validator = $_SESSION['valid'];
$class_checker = mysqli_query($con, "SELECT * FROM login WHERE id = '$page_validator'") or die("error logging in");
$row = mysqli_fetch_assoc($class_checker);
$class = $row['class'];
$location = $row['location'];
$branch = $row['branch'];
$price_list = mysqli_query($con, "SELECT * FROM equipments");
$name_list = mysqli_query($con, "SELECT * FROM equipments");
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<?php


if(isset($_SESSION['valid']))
    {
        ////////important per page
        if ($class==0) {
           ?>
                <meta http-equiv="refresh" content="0;url=dashboard.php?dash=nav5" />
           <?php
        }
        ////////
if (isset($_POST['add_eq'])) {
    $name = $_POST['name'];
    $description = $_POST['description'];
    $price = $_POST['price'];

    mysqli_query($con, "INSERT INTO `equipments`(`equip_name`,`description`,`price`) VALUES ('$name','$description','$price')");
    echo "<script>alert('Add ".$name." as equipments')</script>";
    ?>
        <meta http-equiv="refresh" content="0;url=dashboard.php?dash=nav14" />
    <?php
}
if (isset($_POST['up_price'])) {
    $select_price = $_POST['price_select'];
    $price = $_POST['price'];
    $selector = mysqli_query($con, "SELECT * FROM equipments WHERE id = '$select_price'");
    $selector_row = mysqli_fetch_assoc($selector);
    $name = $selector_row['equip_name'];
    mysqli_query($con, "UPDATE `equipments` SET `price`= '$price' WHERE id = '$select_price'");
    echo "<script>alert('Product ".$name." price is updated to ".$price."')</script>";
    ?>
        <meta http-equiv="refresh" content="0;url=dashboard.php?dash=nav14" />
    <?php
}
if (isset($_POST['up_name'])) {
    $select_price = $_POST['price_select'];
    $price = $_POST['name_list'];
    $selector = mysqli_query($con, "SELECT * FROM equipments WHERE id = '$select_price'");
    $selector_row = mysqli_fetch_assoc($selector);
    $name = $selector_row['equip_name'];
    mysqli_query($con, "UPDATE `equipments` SET `equip_name`= '$price' WHERE id = '$select_price'");
    echo "<script>alert('Product ".$name." price is updated to ".$price."')</script>";
    ?>
        <meta http-equiv="refresh" content="0;url=dashboard.php?dash=nav14" />
    <?php
}
?>
<div class="col-sm-11" style="margin: auto; background-color: white; padding: 25px; border-radius: 5px;">
     <form id="" method="POST" style="background-color: white; padding: 25px; border-radius: 5px;">
        <div class="col-sm">
            <label class="mb-1"><strong>ADD NEW EQUIPMENTS</strong></label>
            <br>
            <div class="row">
                <div class="col-sm">
                    <label class="mb-1"><strong>Name</strong></label>
                    <input type="text" class="form-control" name="name" placeholder="Enter Product Name" required="" style="text-align: center;">
                    <br>
                </div>
                <div class="col-sm">
                    <label class="mb-1"><strong>Description</strong></label>
                    <input type="text" class="form-control" name="description" placeholder="Enter description" required="" style="text-align: center;">
                    <br>
                </div>
                <div class="col-sm">
                    <label class="mb-1"><strong>Price</strong></label>
                    <input type="number" class="form-control" name="price" placeholder="Enter Price" required="" style="text-align: center;">
                    <br>
                </div>
                <div class="col-sm">
                    <div class="form-group">
                        <label class="mb-1"><strong>&nbsp</strong></label>
                        <button type="submit" id="submit" name="add_eq" class="btn btn-primary btn-block" style="background-color: hsla(21, 51%, 17%, 1); border-color: white; width: 150px; margin: auto; ">Save</button>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <form id="" method="POST" style="background-color: white; padding: 25px; border-radius: 5px;">
        <div class="col-sm">
            <label class="mb-1"><strong>UPDATE PRICE</strong></label>
            <div class="row">
                <div class="col-sm">
                    <label class="mb-1"><strong>Equipment Name</strong></label>
                    <select name = "price_select" class="form-control">
                    <?php 
                       while($category_list_row3 = mysqli_fetch_array($price_list)):;
                    ?>
                        <option value="<?php echo $category_list_row3[0];?>">
                        <?php echo ucfirst($category_list_row3[1]);?>
                        </option>
                    <?php 
                        endwhile;
                    ?>
                    </select>
                </div>
                <div class="col-sm">
                    <label class="mb-1"><strong>New Price</strong></label>
                    <input type="text" class="form-control" name="price" placeholder="Enter New Price" required="" style="text-align: center;">
                </div>
                <div class="col-sm">
                    <div class="form-group">
                    <label class="mb-1"><strong>&nbsp</strong></label>
                    <button type="submit" id="submit" name="up_price" class="btn btn-primary btn-block" style="background-color:hsla(21, 51%, 17%, 1); border-color: white; width: 150px; margin: auto; ">Confirm</button>
                    </div>
                </div>
                <div class="col-sm">
                </div>
            </div>
        </div>
    </form>
    <form id="" method="POST" style="background-color: white; padding: 25px; border-radius: 5px;">
        <div class="col-sm">
            <label class="mb-1"><strong>UPDATE EQUIPMENT NAME</strong></label>
            <div class="row">
                <div class="col-sm">
                    <label class="mb-1"><strong>Equipment Name</strong></label>
                    <select name = "price_select" class="form-control">
                    <?php 
                       while($category_list_row3 = mysqli_fetch_array($name_list)):;
                    ?>
                        <option value="<?php echo $category_list_row3[0];?>">
                        <?php echo ucfirst($category_list_row3[1]);?>
                        </option>
                    <?php 
                        endwhile;
                    ?>
                    </select>
                </div>
                <div class="col-sm">
                    <label class="mb-1"><strong>New Name</strong></label>
                    <input type="text" class="form-control" name="name_list" placeholder="Enter New Name" required="" style="text-align: center;">
                </div>
                <div class="col-sm">
                    <div class="form-group">
                    <label class="mb-1"><strong>&nbsp</strong></label>
                    <button type="submit" id="submit" name="up_name" class="btn btn-primary btn-block" style="background-color:hsla(21, 51%, 17%, 1); border-color: white; width: 150px; margin: auto; ">Confirm</button>
                    </div>
                </div>
                <div class="col-sm">
                </div>
            </div>
        </div>
    </form>
</div>

<?php
    }
else
    {
        header("location: index.php");
    }
?>
</body>
</html>