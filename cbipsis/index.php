<?php session_start();
error_reporting(1);
include("connection.php");?>
<!DOCTYPE html>
<style type="text/css">
    html, body {
        height: 100%;
        margin: 0;
    }
    .bg {
        background:linear-gradient( rgba(25, 26, 14, 0.15) 100%, rgba(25, 26, 14, 0.15)100%),url('des/des/images/wallpaper.jpg');
        height: 100%; 
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }
    img {
      width: 50%;
      height: auto;
    }
    ::placeholder {
      color: rgba(6, 55, 28, 1);
    }
    .button_front button:hover {
        background-color: white;
        color: rgba(11, 79, 91, 1) ;
    }
    .button_front button {
        background-color: hsla(42, 27%, 52%, 1);
    }

     @media screen and (max-width: 750px)
        {
            .index-hide{
                display: none;
            }
        }
</style>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cloud Based Interbranch Poultry Sales Information System</title>
    <link href="design/css/design.css" rel="stylesheet">
    <link rel="shortcut icon" href="des/des/images/head.png"/>
    <link href="des/des/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="des/des/vendor/select2/css/select2.min.css">
    <link href="des/des/vendor/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet"> 
</head>
<body>  
    <div class="bg">
        <div class="container h-100">
            <div class="row justify-content-center h-100 align-items-center">
                <div class="col-md-5 index-hide">
                </div>
                <div class="col-md-5">
                    <div class="authincation-content" style="box-shadow: 3px 3px 8px hsla(212, 0%, 46%, 1), -3px -3px 8px hsla(212, 0%, 46%, 1);background-color: rgba(63, 233, 134, 0); background-image: linear-gradient(to right, hsla(42, 27%, 71%, 1),hsla(90, 13%, 100%, 0.35));">
                        <div class="row no-gutters">
                            <div class="col-xl-12">
                                <div class="auth-form">
                                    <h4 class="text-center mb-4" style="color: white;"><strong><img src="design/a.png"></strong></h4>
                                    <form id="login" name="login" method="post">
                                        <div class="form-group">
                                            <div class="form-control" style=" text-align: center; margin-bottom: 5px;border-radius: 25px;box-shadow: inset 8px 8px 8px #cbced1, inset -8px -8px 8px #ffffff;">
                                                <i class="fa fa-user" style="color: rgba(6, 55, 28, 1);" aria-hidden="true"></i>
                                                <input type="username" style="width: 85%; border: 1px solid rgba(63, 207, 233, 0); text-align: center; background-color: rgba(63, 207, 233, 0); color: rgba(6, 55, 28, 1); " id="username" name="username"  placeholder="Username">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-control" style=" text-align: center; margin-bottom: 5px;border-radius: 25px;box-shadow: inset 8px 8px 8px #cbced1, inset -8px -8px 8px #ffffff;">
                                                <i class="fa fa-key" style="color: rgba(6, 55, 28, 1);" aria-hidden="true"></i>
                                                <input type="Password" style="width: 85%; border: 1px solid rgba(63, 207, 233, 0); text-align: center; background-color: rgba(63, 207, 233, 0); color: rgba(6, 55, 28, 1); " id="password" name="password"  placeholder="Password">
                                            </div>
                                        </div>
                                        <?php
                                            $text = $_GET['text'];

                                            if (isset($_SESSION['valid'])) {
                                                $login_id = $_SESSION['valid'];
                                                $login_checker = mysqli_query($con, "SELECT * FROM login WHERE id = '$login_id' ");
                                                $login_checker_row = mysqli_fetch_assoc($login_checker);
                                                $class = $login_checker_row['class'];
                                                if ($class==0) {
                                                    header("location: dashboard.php?dash=nav5");
                                                } else if ($class==1) {
                                                     header("location: dashboard.php?dash=nav7");
                                                } else {
                                                    header("location: dashboard.php?dash=nav25");
                                                }
                                            } else {
                                                if (isset($_POST['login'])) {
                                                    $username = strtolower($_POST['username']);
                                                    $password = md5($_POST['password']);

                                                    $login_sql = mysqli_query($con, "SELECT * FROM login WHERE username = '$username'");
                                                    $login_row = mysqli_fetch_assoc($login_sql);
                                                    $sqlun = $login_row['username'];
                                                    $sqlpass = $login_row['password'];
                                                    $sqlloginid = $login_row['id'];
                                                    $sqllogincat = $login_row['class'];

                                                    if($sqlun==$username && $sqlpass==$password)
                                                        {
                                                            $_SESSION['valid'] = $sqlloginid;
                                                            
                                                            if ($sqllogincat==0) {
                                                                header("location: dashboard.php?dash=nav5");
                                                            } else if ($sqllogincat==1) {
                                                                 header("location: dashboard.php?dash=nav7");
                                                            } else {
                                                                header("location: dashboard.php?dash=nav25");
                                                            }
                                                        }
                                                    else
                                                        {
                                                            echo "<p style='color:red;'>username or password are incorrect</p>";
                                                        }
                                                }
                                            }
                                        if (!empty($text)) {
                                            echo "<p style='color:green;'>",$text;
                                        }

                                        ?>
                                        <div class="form-row d-flex justify-content-between mt-4 mb-2">
                                            <div class="form-group">
                                               <div class="custom-control custom-checkbox ml-1">
                                                    <input type="checkbox" class="custom-control-input" id="basic_checkbox_1" onclick="showPassword()">
                                                    <label class="custom-control-label" for="basic_checkbox_1" style="color: rgba(6, 55, 28, 1);"> Show Password</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-center button_front">
                                            <button type="submit" id="submit" name="login" class="btn btn-primary btn-block" style=" border: 1px solid rgba(63, 207, 233, 0); ">Log in</button>
                                        </div>
                                    </form>
                                    <div class="new-account mt-3">
                                        <a class="active" href='forgot.php' style="color: rgba(6, 55, 28, 1);"> Forgot Password?</a><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="vendor/global/global.min.js"></script>
    <script src="vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="js/custom.min.js"></script>
    <script src="js/deznav-init.js"></script>
    <script>
    function showPassword() {
      var x = document.getElementById("password");
      if (x.type === "password") {
        x.type = "text";
      } else {
        x.type = "password";
      }
    }
    </script>
</body>
</html>