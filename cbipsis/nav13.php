<?php session_start();
error_reporting(1);
date_default_timezone_set('Asia/Manila');
include("connection.php");
$date = ucfirst(date('F j, Y'));

$page_validator = $_SESSION['valid'];
$class_checker = mysqli_query($con, "SELECT * FROM login WHERE id = '$page_validator'") or die("error logging in");
$row = mysqli_fetch_assoc($class_checker);
$class = $row['class'];
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<?php
$bn_list = mysqli_query($con, "SELECT * FROM branch");
$bn_no = mysqli_query($con, "SELECT * FROM branch_no");

if (isset($_POST['del_loc'])) {
$branch_name2 = $_POST['branch_name'];
$name_branch = mysqli_query($con, "SELECT * FROM branch WHERE id = '$branch_name2'");
$branch_name2_fetch = mysqli_fetch_assoc($name_branch);
$branch_name2_release = $branch_name2_fetch['branch_name'];
?>
    <form class="popup" method="post">
        <div class="popup-content">
        <head>Current Address( <?php echo $branch_name2_release;?> )</head>
        <br>
        <br>
        <input type="text" name="new_loc_name" autofocus="" style="text-align: center;" placeholder="Enter New Address">
        <input type="hidden" name="getter_loc_id" value="<?php echo $branch_name2;?>">
        <br>
        <br>
        <button name="confirm_new_loc_name">Confirm</button>
        <button name="">Cancel</button>
        </div>
    </form>
<?php
}
if (isset($_POST['confirm_new_loc_name'])) {
    $branch_name = ucfirst($_POST['new_loc_name']);
    $getter_loc_id = $_POST['getter_loc_id'];

    if ($branch_name=="") {
        echo "<script>alert('Text box is empty')</script>";
    } else {
        $checker = mysqli_query($con, "UPDATE `branch` SET `branch_name`='$branch_name' WHERE id ='$getter_loc_id'") or die("<script>alert('$branch_name is already exist')</script><meta http-equiv='refresh' content='0;url=dashboard.php?dash=nav13' />");

        echo "<script>alert('Successfully updated to ".$branch_name."')</script>";
        ?>
            <meta http-equiv="refresh" content="0;url=dashboard.php?dash=nav13" />
        <?php
    }
}
if (isset($_POST['del_no'])) {
$branch_no = $_POST['branch_no_no'];
$branch_number = mysqli_query($con, "SELECT * FROM branch_no WHERE id = '$branch_no'");
$branch_number_row = mysqli_fetch_assoc($branch_number);
$branch_number_number = $branch_number_row['branch_no'];
?>
    <form class="popup" method="post">
        <div class="popup-content">
        <head>Current Branch no.( <?php echo $branch_number_number;?> )</head>
        <br>
        <br>
        <input type="text" name="new_brn_name" autofocus="" style="text-align: center;" placeholder="Enter New Branch no.">
        <input type="hidden" name="getter_brn_id" value="<?php echo $branch_no;?>">
        <br>
        <br>
        <button name="confirm_new_brn_name">Confirm</button>
        <button name="">Cancel</button>
        </div>
    </form>
<?php
}
if (isset($_POST['confirm_new_brn_name'])) {
    $branch_name = ucfirst($_POST['new_brn_name']);
    $getter_loc_id = $_POST['getter_brn_id'];

    if ($branch_name=="") {
        echo "<script>alert('Text box is empty')</script>";
    } else {
        $checker = mysqli_query($con, "UPDATE `branch_no` SET `branch_no`='$branch_name' WHERE id ='$getter_loc_id'") or die("<script>alert('Branch no $branch_name is already exist')</script><meta http-equiv='refresh' content='0;url=dashboard.php?dash=nav13' />");

        echo "<script>alert('Successfully updated to ".$branch_name."')</script>";
        ?>
            <meta http-equiv="refresh" content="0;url=dashboard.php?dash=nav13" />
        <?php
    }
}

if (isset($_POST['add_loc'])) {
    $location = ucfirst($_POST['location']);
    if ($location=="") {
        echo "<script>alert('Text box is empty')</script>";
    } else {
        mysqli_query($con, "INSERT INTO `branch` (`branch_name`) VALUES ('$location')");
        echo "<script>alert('Add ".$location." as branch list')</script>";
        ?>
            <meta http-equiv="refresh" content="0;url=dashboard.php?dash=nav13" />
        <?php
    }
}
if (isset($_POST['del_loc_lit'])) {
    $branch_no = $_POST['branch_no_no'];
    $checker = mysqli_query($con, "SELECT * FROM login WHERE branch = '$branch_no' ");
    $checker_rows = mysqli_num_rows($checker);
    if ($checker_rows>0){
       echo "<script>alert('Active branch cannot be deleted')</script>";
    } else {
        
        mysqli_query($con, "DELETE FROM `branch_no` WHERE id = $branch_no ");
        echo "<script>alert('Delete branch no. ".$branch_no." Successfully')</script>";
        ?>
            <meta http-equiv="refresh" content="0;url=dashboard.php?dash=nav13" />
        <?php
    }
}
///////////////////////
if (isset($_POST['add_no'])) {
    $location = ucfirst($_POST['branch_no']);
    if ($location=="") {
        echo "<script>alert('Text box is empty')</script>";
    } else {
        mysqli_query($con, "INSERT INTO `branch_no` (`branch_no`) VALUES ('$location')");
        echo "<script>alert('Add ".$location." in branch no list')</script>";
        ?>
            <meta http-equiv="refresh" content="0;url=dashboard.php?dash=nav13" />
        <?php
    }
}
if(isset($_SESSION['valid']))
    {
        ////////important per page
        if ($class==0) {
           ?>
                <meta http-equiv="refresh" content="0;url=dashboard.php?dash=nav5" />
           <?php
        }
        ////////
        ?>
<div class="row">
    <div class="col-sm-12" style=" margin: auto;">
        <div class="col-sm-11" style="background-color: white; padding: 20px; border-radius: 5px; margin: auto;">
            <!------------------------------------------------------->
            <!------------------------------------------------------->
            <div id="add_stock"  style="background-color: white; padding: 20px; border-radius: 5px;">
                <div class="table-responsive" style="max-height: 300px;">
                    <header><strong>BRANCHES</strong></header>
                    <br>
                    <table class="table table-responsve-sm-5 mb-0" style="">
                        <thead>
                            <td><strong>Branch no.</strong></td>
                            <td><strong>Address</strong></td>
                            <td><strong>No. of Employees</strong></td>
                        </thead>
                    <?php
                        $employee_counter = mysqli_query($con , "SELECT * FROM branch WHERE activity = 0");
                        $employee_counter_num_count = mysqli_num_rows($employee_counter);
                        if ($employee_counter_num_count>0) {
                            while ($rows_e_counter = mysqli_fetch_assoc($employee_counter)) {
                            $branch_name_row = $rows_e_counter['branch_name'];

                                $login_counter = mysqli_query($con, "SELECT DISTINCT location, branch FROM login WHERE location = '$branch_name_row' AND blocked = '0' ORDER BY location ASC ");
                                while ($login_counter_row = mysqli_fetch_assoc($login_counter)) {
                                    $lbranch = $login_counter_row['branch'];
                                    $llocation = $login_counter_row['location'];

                                    $counter_of_no_employed = mysqli_query($con, "SELECT * FROM login WHERE branch = '$lbranch' AND location = '$llocation'");
                                    $num_rows_count = mysqli_num_rows($counter_of_no_employed);

                                    ?>
                                    <tr>
                                        <td><?php echo $lbranch;?></td>
                                        <td><?php echo $llocation;?></td>
                                        <td><?php echo $num_rows_count;?></td>
                                    </tr>
                                    <?php
                                }    
                            }
                        } else {
                            echo "No data";
                            ?>
                            <tr>
                                <td><strong>No</strong></td>
                                <td><strong>Data</strong></td>
                                <td><strong></strong></td>
                                <td><strong></strong></td>
                            </tr>
                            <?php
                        }
                    ?>
                    </table>
                </div>
            </div>
            <hr>
            <br>
            <style type="text/css">
                .nav13 label {
                    width: 140px;
                    font-size: 12px;
                    text-align: left;
                }
                .nav13 input {
                    width: 220px;
                    height: 40px;
                    background-color: white;
                    border: 1px solid lightgray;
                    border-radius: 5px;
                    text-align: center;
                }
                .nav13 {
                   margin-bottom: 5px;
                }

                @media screen and (min-width: 750px)
                    {
                        .hides{
                            display: none;
                        }
                    }
                @media screen and (min-width: 750px)
                    {
                        .methodology{
                           width: 50%; margin: auto;
                        }
                    }
            </style>
            <div class="col-sm-12" style="">
                <form class="methodology" method="post" style="">
                    <div class="nav13 button">
                        <label class="mb-1"><strong>Add New Address</strong></label>
                        <div class="hides"><br></div>
                        <input type="text" class="" name="location" placeholder="Enter Address Ex. Manila" style="text-align: center;">
                        <div class="hides"><br></div>
                        <button type="submit" id="submit" name="add_loc" class="" style=" background-color: white; border: none;"><i class="fa fa-save" style="color: black; background-color: white; font-size: 20px"></i></button>
                        <div class="hides"><br></div>
                    </div>
                    <!--------------------------------------------------------------------->
                    <div class="nav13">
                        <label class="mb-1"><strong>Update Address</strong></label>
                        <div class="hides"><br></div>
                        <select name = "branch_name" class="" style="">
                        <?php 
                           while($branch_row = mysqli_fetch_array($bn_list)):;
                        ?>
                            <option value="<?php echo $branch_row[0];?>">
                            <?php echo ucfirst($branch_row[1]);?>
                            </option>
                        <?php 
                            endwhile;
                        ?>
                        </select>
                        <div class="hides"><br></div>
                        <button type="submit" id="submit" name="del_loc" class="" style=" background-color: white; border: none;"><i class="fa fa-edit" style="color: black; background-color: white; font-size: 20px"></i></button>
                    </div>
                    <!--------------------------------------------------------------------->
                    <div class="nav13 button">
                        <div class="hides"><br></div>
                        <label class="mb-1"><strong>Add Branch No.</strong></label>
                        <div class="hides"><br></div>
                        <input type="text" class="" name="branch_no" placeholder="Enter Branch no. ex. 1" style="text-align: center;">
                        <div class="hides"><br></div>
                        <button type="submit" id="submit" name="add_no" class="" style=" background-color: white; border: none;"><i class="fa fa-save" style="color: black; background-color: white; font-size: 20px"></i></button>
                    </div>
                    <!--------------------------------------------------------------------->
                    <div class="nav13">
                        <div class="hides"><br></div>
                        <label class="mb-1"><strong>Update Branch No.</strong></label>
                        <div class="hides"><br></div>
                        <select name = "branch_no_no" class="">
                        <?php 
                           while($branch_no_row = mysqli_fetch_array($bn_no)):;
                        ?>
                            <option value="<?php echo ucfirst($branch_no_row[0]);?>">
                            <?php echo ucfirst($branch_no_row[1]);?>
                            </option>
                        <?php 
                            endwhile;
                        ?>
                        </select>
                        <div class="hides"><br></div>
                        <button type="submit" id="submit" name="del_no" class="" style=" background-color: white; border: none;"><i class="fa fa-edit" style="color: black; background-color: white; font-size: 20px"></i></button>
                        <button type="submit" id="submit" name="del_loc_lit" class="" style=" background-color: white; border: none;"><i class="fa fa-trash" style="color: black; background-color: white; font-size: 20px"></i></button>
                    </div>
                    <!--------------------------------------------------------------------->
                </form>
            </div>
            </div>
        </div>
    </div>
</div>
<?php

    }
else
    {
        header("location: index.php");
    }
?>
<style type="text/css">
    .popup {
        position: absolute;
        z-index: 1;
        width: 75%;
        text-align: center;
    }
    .popup-content {
        background-color: white;
        box-shadow: 1px 1px 1px 1px gray,1px 1px 1px 1px gray;
        width: 300px;
        text-align: center;
        padding: 20px;
        border-radius: 10px;
        margin: auto;
        margin-top: 150px;
    }

    .popup-content input {
        border-radius: 5px;
    }

    .popup-content button {
        background-color: hsla(21, 51%, 17%, 1);
        color: white;
        border-radius: 10px;
        width: 80px;
        border:none;
    }
    .popup-content button:hover {
        box-shadow: 1px 1px 2px 2px brown,1px 1px 2px 2px brown;
    }
    @media screen and (max-width: 750px)
        {
            .popup-content {
                background-color: white;
                box-shadow: 1px 1px 1px 1px gray,1px 1px 1px 1px gray;
                margin-left: 15px;
            }
        }
</style>
</body>
</html>