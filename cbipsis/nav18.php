<?php session_start();
error_reporting(1);
date_default_timezone_set('Asia/Manila');
include("connection.php");
$date = ucfirst(date('F j, Y'));
$page_validator = $_SESSION['valid'];
$class_checker = mysqli_query($con, "SELECT * FROM login WHERE id = '$page_validator'") or die("error logging in");
$row = mysqli_fetch_assoc($class_checker);
$class = $row['class'];
$location = $row['location'];
$branch = $row['branch'];
$bn_list = mysqli_query($con, "SELECT * FROM branch");
$bn_no = mysqli_query($con, "SELECT * FROM branch_no");
$getid = $_GET['id'];
$getloc = $_GET['loc'];
$getbranch = $_GET['branch'];
$box_name_fetch = mysqli_query($con, "SELECT * FROM equipments WHERE id = '$getid'");
$box_name_row = mysqli_fetch_assoc($box_name_fetch);
$box_name = strtoupper($box_name_row['equip_name']);
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<?php
if(isset($_SESSION['valid']))
    {
        ////////important per page
        if ($class==0) {
           ?>
	        <style type="text/css"> #nav3,#nav-5-1 {color: white; font-weight: bold; background-color:gray;} #nav-5-1{padding: 8px; border-radius: 2px; position: static;} #nav3:hover,#nav-5-1:hover { color: white; background-color: black; text-shadow: none;}</style>
	        <?php
        } else {
        	?>
	        <style type="text/css"> #nav3,#nav-3-2 {color: white; font-weight: bold; background-color:gray;} #nav-3-2{padding: 8px; border-radius: 2px; position: static;} #nav3:hover,#nav-3-2:hover { color: white; background-color: black; text-shadow: none;}</style>
	        <?php
        }
        ////////
     	if ($getloc=="") {
     		$header_info = "";
     	} else {
     		$header_info = strtoupper($getloc);
     	}
		?>
		<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-11" style="margin: auto;">
		<form id="add_stock" method="POST" style="background-color: white; padding: 25px; border-radius: 5px;">
		<label class="mb-1"><strong><?php echo $box_name;?> <?php echo $header_info;?> STOCK</strong></label>
		<br>
		<br>
		<div class="row">
		<div class="col-sm"></div>
		<div class="col-sm-3">
		    <div class="form-group">
		        <label class="mb-1"><strong>Location</strong></label>
		        <select name="change" id="change" onChange="doReload(this.value);" class="form-control">
                        <option>--Select Location--</option>
                        <option value="id=<?php echo $getid?>">All</option>
                    <?php
                   while($branch_row = mysqli_fetch_array($bn_list)):;
                    ?>
                        <option value="id=<?php echo $getid?>&loc=<?php echo ucfirst($branch_row[1]);?>&branch=">
                        <?php echo ucfirst($branch_row[1]);?>
                        </option>
                    <?php 
                        endwhile;
                    ?>
                </select>
                <script language="javascript" type="text/javascript">
                function doReload(change){
                    document.location = 'dashboard.php?dash=nav17&' + change;
                }
                </script>
		        <!---->
		    </div>
		    <div class="form-group">
		        <label class="mb-1"><strong>Branch Number</strong></label>
		        <select name="change" id="change" onChange="doReload(this.value);" class="form-control">
                        <option>--Select Branch--</option>
                        <option value="id=<?php echo $getid?>&loc=<?php echo $getloc;?>&branch=">All</option>
                    <?php
                   	while($branch_no_row = mysqli_fetch_array($bn_no)):;
                    ?>
                        <option value="id=<?php echo $getid?>&loc=<?php echo ucfirst($getloc);?>&branch=<?php echo ucfirst($branch_no_row[1]);?>">
	                    <?php echo ucfirst($branch_no_row[1]);?>
	                    </option>
                    <?php 
                        endwhile;
                    ?>
                </select>
                <script language="javascript" type="text/javascript">
                function doReload(change){
                    document.location = 'dashboard.php?dash=nav18&' + change;
                }
                </script>
		    </div>
		</div>
		<div class="col-sm-8">
		   <div class="table-responsive" style="max-height: 400px;">
            <table class="table table-responsive-sm-8 mb-0" style="">
                <thead>
                    <tr style="text-align: center;">
                        <th><strong>Location</strong></th>
                        <th><strong>Branch</strong></th>
                        <th><strong>Stock</strong></th>
                    </tr>
                </thead>
                <?php
          				if ($getloc=="") {
	                    	$location_sql = "";
	                    } else {
	                    	$location_sql = "WHERE branch_name ='$getloc'";
	                    }
	                $notif_no_stock = mysqli_query($con, "SELECT * FROM item_stock WHERE item_id = '$getid'");
	                $notif_check = mysqli_num_rows($notif_no_stock);
	                if ($notif_check>0) {
	                	/////////////////////////////
	                    $branch = mysqli_query($con, "SELECT * FROM branch $location_sql");
	                    $branch_result_num_row = mysqli_num_rows($branch);
	                    if ($getbranch=="") {
	                    		$branch_number_list = "All";
	                    	} else {
	                    		$branch_number_list =$getbranch;
	                    	}
	                    	//////////////////////////////// startng while sa branch
	                        while ($branch_row = mysqli_fetch_assoc($branch)) {
	                            $branch_location = $branch_row['branch_name'];
	                            if ($branch_result_num_row>0) {
		                            if ($getbranch== "") {
			                    		$branch_number_sql = "WHERE location ='$branch_location' AND item_id = '$getid'";
			                    	} else {
			                    		$branch_number_sql = "WHERE location ='$getloc' AND branch = '$getbranch' AND item_id = '$getid'";
			                    	}
		                            $category_fetch = mysqli_query($con, "SELECT * FROM item_stock $branch_number_sql ");
		                            $change_text = mysqli_num_rows($category_fetch);
		                            /////////////////// start ng change text
		                            if ($change_text>0) {
								   		$category_fetch_row = mysqli_fetch_assoc($category_fetch);
			                            $id2 = $category_fetch_row['id'];
			                            $description2 = $category_fetch_row['location'];
				                        $quantity_fetch = mysqli_query($con, "SELECT SUM(quantity) AS value_sum FROM item_stock WHERE location = '$description2' AND item_id = '$getid'");
									    $quantity_row = mysqli_fetch_assoc($quantity_fetch);
									    $quantity = $quantity_row['value_sum'];
									    $quantity_fetch2 = mysqli_query($con, "SELECT SUM(quantity) AS value_sum FROM item_sold WHERE location = '$description2' AND item_id = '$getid'");
									    $quantity_row2 = mysqli_fetch_assoc($quantity_fetch2);
									    $quantity2 = $quantity_row2['value_sum'];
			                                ?>
			                                <form id="" method="POST">
			                                    <input type="hidden" name="id" value="<?php echo $id?>">
			                                    <tr style="text-align: center;">
			                                        <td><?php echo ucfirst($description2);?></td>
			                                        <td><?php echo ucfirst($branch_number_list);?></td>
			                                        <td><?php echo ucfirst($quantity-$quantity2);?></td>
			                                    </tr>
			                                </form>
			                                <?php
								   	} else {
									   	
								   	}//////////////// END NG change text >0
							   	} else {
			                        ?>
			                            <tr>
			                                <td>
			                                    <p style="color:gray;"><?php echo $search?> is not exist</p>
			                                </td>
			                            </tr>
			                        <?php
		                    	}
	                        }
	                        /////////////////// start ng change text
	                        		if ($getbranch== "") {
			                    		$branch_number_sql9 = "WHERE location ='$branch_location' AND item_id = '$getid'";
			                    	} else {
			                    		$branch_number_sql9 = "WHERE location ='$branch_location' AND branch = '$getbranch' AND item_id = '$getid'";
			                    	}
		                            $category_fetch9 = mysqli_query($con, "SELECT * FROM item_stock $branch_number_sql9 ");
		                            $change_text9 = mysqli_num_rows($category_fetch9);
		                            if ($change_text9>0) {
								   	} else {
									   	?>
				                         <tr>
			                                <td>
			                                    <p style="color:gray;"><?php echo $getloc;?> branch <?php echo $getbranch ;?> is out of stock</p>
			                                </td>
			                            </tr>
				                        <?php
								   	}//////////////// END NG change text >0
	                        ////////////////////////////////////end ng while sa branch
	                } else {
	                	?>
	                		<tr>
                                <td>
                                    <p style="color:gray;">all branch is out of stock</p>
                                </td>
                            </tr>
	                	<?php
	                }  
            ?>
            </table>
        </div>
        	<?php
        	$quantity_fetch3 = mysqli_query($con, "SELECT SUM(quantity) AS value_sum FROM item_stock WHERE item_id = '$getid'");
		    $quantity_row3 = mysqli_fetch_assoc($quantity_fetch3);
		    $quantity3 = $quantity_row3['value_sum'];
		    $quantity_fetch4 = mysqli_query($con, "SELECT SUM(quantity) AS value_sum FROM item_sold WHERE item_id = '$getid'");
		    $quantity_row4 = mysqli_fetch_assoc($quantity_fetch4);
		    $quantity4 = $quantity_row4['value_sum'];
        	?>
        	<p style="color:gray; text-align: right;"><strong>All Branch Total Stock : <?php echo $quantity3-$quantity4;?></strong></p><strong>                               
		</div>
		<div class="col-sm"></div>
		</div>
		<div class="row">
		<div class="col-sm"></div>
		<div class="col-sm">
		<div class="form-group">
		    <a type="submit" id="submit" name="cancel" class="btn btn-primary btn-block" style="background-color: #787312; border-color: #a1990b; box-shadow: 3px 3px 8px #b1b1b1, -3px -3px 8px #ffffff; width: 150px; margin: auto; " href = "dashboard.php?dash=nav16">OTHERS</a>
		</div>
		</div>
		<div class="col-sm"></div>
		</div>
		</form>
		</div>
		<div class="col-sm-2"></div>
		</div>
		<?php
    }
else
    {
        header("location: index.php");
    }
?>
</body>
</html>