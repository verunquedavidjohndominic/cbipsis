<?php session_start();
error_reporting(1);
date_default_timezone_set('Asia/Manila');
include("connection.php");
$date = ucfirst(date('Y-m-d'));

$page_validator = $_SESSION['valid'];
$class_checker = mysqli_query($con, "SELECT * FROM login WHERE id = '$page_validator'") or die("error logging in");
$row = mysqli_fetch_assoc($class_checker);
$class = $row['class'];
$location = $row['location'];
$branch = $row['branch'];
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<?php


if(isset($_SESSION['valid']))
    {
        ////////important per page
        if ($class==0) {
           ?>
                <meta http-equiv="refresh" content="0;url=dashboard.php?dash=nav5" />
           <?php
        }
        ////////
        ?>
        <div class="row">
            <div class="col-sm-11" style="margin: auto;">
                <!------------------------------------------------------->
                <!------------------------------------------------------->
                <div id="add_stock"  style="background-color: white; padding: 5px; border-radius: 5px;">
                    <div style="text-align: right;">
                        <?php
                            nav7($con);
                        ?>
                        <!------------------------------------------------------->
                        <!------------------------------------------------------->
                        <!------------------------------------------------------->
                        <div class="row" style="margin-top: -30px;">
                            <?php
                                $getloc = $_GET['loc'];
                                $getbranch = $_GET['branch'];
                                $getstatus = $_GET['status'];
                                //////////////////////////
                                $date_counter = date('Y-m-d',time()-(7*86400));
                                $date_counter1 = date('Y-m-d',time()-(31*86400));
                                $date_counter2 = date('Y-m-d',time()-(365*86400));
                                ////////////////////////
                                if ($getstatus==4) {
                                   $date_status = "`date` = '$date'";
                                } else if ($getstatus==1) {
                                    $date_status = "`date` >= '$date_counter' AND `date` <= '$date'";
                                } elseif ($getstatus==2){
                                    $date_status = "`date` >= '$date_counter1' AND `date` <= '$date'";
                                } elseif ($getstatus==3){
                                    $date_status = "`date` >= '$date_counter2' AND `date` <= '$date'";
                                } else {
                                    $date_status = "`date` = '$getstatus'";
                                }
                                ////////////////////////
                                if ($getloc==""&&$getstatus==""&&$getbranch=="") {
                                    $where = "WHERE `date` = '$date' ";
                                }
                                //////////////////////////////
                                if ($getloc!="") {
                                    if ($getloc=="All") {
                                        $where = "";
                                    } else {
                                        $where = "WHERE location = '$getloc' AND `date` = '$date' ";
                                    }
                                }
                                ////////////////////
                                if ($getbranch!="") {
                                    $where = "WHERE branch = '$getbranch' AND `date` = '$date' ";
                                }
                                if ($getstatus!="") {
                                    $where = "WHERE $date_status";
                                }
                                ///////////////////////////
                                if ($getloc!=""&&$getbranch!="") {
                                    if ($getloc=="All") {
                                        $where = "WHERE branch = '$getbranch'";
                                    } else {
                                        $where = "WHERE branch = '$getbranch' AND location = '$getloc' AND `date` = '$date'";
                                    }
                                }
                                if ($getstatus!=""&&$getbranch!="") {
                                    $where = " WHERE branch = '$getbranch' AND $date_status";
                                }
                                if ($getstatus!=""&&$getloc!="") {
                                    if ($getloc=="All") {
                                        $where = "WHERE AND $date_status";
                                    } else {
                                        $where = "WHERE location = '$getloc' AND $date_status";
                                    }
                                }
                                if ($getstatus!=""&&$getloc!=""&&$getbranch!="") {
                                    $where = " WHERE location = '$getloc' AND branch = '$getbranch' AND $date_status";
                                }
                                /////////////////////////////// fetched
                                $sold_fetched = mysqli_query($con, "SELECT item_id, SUM(quantity) AS sold_sum FROM product_sold $where GROUP BY item_id ORDER BY SUM(quantity) DESC LIMIT 5 ");
                                $capture_num_row = mysqli_num_rows($sold_fetched);
                            ?>
                            <div class="col-sm-7" >
                                <div class="col-sm-10" style="margin: auto; font-size: 12px; text-align: left; padding: 10px; border-radius: 5px; background-color:rgba(0, 15, 13, 0.04); margin: auto;">
                                    <?php
                                    ////////////////////////////// while fetched start
                                    while ($sold_fetched_row = mysqli_fetch_assoc($sold_fetched)) {
                                        $total_quantity = $sold_fetched_row['sold_sum'];
                                        $item_id_fetched = $sold_fetched_row['item_id'];

                                        $item_info_fetched = mysqli_query($con, "SELECT * FROM products WHERE id ='$item_id_fetched' ");
                                        $item_info_fetched_row = mysqli_fetch_assoc($item_info_fetched);
                                        $item_name = $item_info_fetched_row['product_name'];
                                        $item_price = $item_info_fetched_row['price'];

                                        $total_price = $total_quantity*$item_price;

                                        $p_total += $total_price; 
                                    }
                                    ////////////////////////////// while fetched end
                                    $sold_fetched = mysqli_query($con, "SELECT item_id, SUM(quantity) AS sold_sum FROM item_sold $where GROUP BY item_id ORDER BY SUM(quantity) DESC LIMIT 5 ");
                                    $capture_num_row = mysqli_num_rows($sold_fetched);
                                    ////////////////////////////// while fetched start
                                    while ($sold_fetched_row = mysqli_fetch_assoc($sold_fetched)) {
                                        $total_quantity = $sold_fetched_row['sold_sum'];
                                        $item_id_fetched = $sold_fetched_row['item_id'];

                                        $item_info_fetched = mysqli_query($con, "SELECT * FROM equipments WHERE id ='$item_id_fetched' ");
                                        $item_info_fetched_row = mysqli_fetch_assoc($item_info_fetched);
                                        $item_name = $item_info_fetched_row['equip_name'];
                                        $item_price = $item_info_fetched_row['price'];

                                        $total_price = $total_quantity*$item_price;
                                        
                                        $i_total += $total_price;
                                    }
                                    ////////////////////////////// while fetched end
                                        $totality = $p_total+$i_total;
                                    ?>
                                    <header style="text-align: right;"><strong><?php echo strtoupper($getloc);?> <?php echo strtoupper($getbranch); ?> TOTAL SALE : <?php echo $totality; ?></strong></header>
                                    <!------------------------------>
                                    <!------------------------------>
                                    <!------------------------------>
                                    <div class="table-responsive" style="max-height: 400px;">
                                        <table class="table table-responsive-sm-5 mb-0" style="">
                                            <header><strong>TOP 5 SELDOM SELLING PRODUCT</strong></header>
                                            <thead>
                                                <tr style="text-align: left;">
                                                    <td><strong>Name</strong></td>
                                                    <td><strong>Quantity</strong></td>
                                                    <td><strong>Sales</strong></td>
                                                </tr>
                                            </thead>
                                        <?php
                                        $sold_fetched = mysqli_query($con, "SELECT item_id, SUM(quantity) AS sold_sum FROM product_sold $where GROUP BY item_id ORDER BY SUM(quantity) ASC LIMIT 5 ");
                                        $capture_num_row = mysqli_num_rows($sold_fetched);
                                        if ($capture_num_row>0) {
                                            ////////////////////////////// while fetched start
                                            while ($sold_fetched_row = mysqli_fetch_assoc($sold_fetched)) {
                                                $total_quantity = $sold_fetched_row['sold_sum'];
                                                $item_id_fetched = $sold_fetched_row['item_id'];

                                                $item_info_fetched = mysqli_query($con, "SELECT * FROM products WHERE id ='$item_id_fetched' ");
                                                $item_info_fetched_row = mysqli_fetch_assoc($item_info_fetched);
                                                $item_name = $item_info_fetched_row['product_name'];
                                                $item_price = $item_info_fetched_row['price'];

                                                $total_price = $total_quantity*$item_price;
                                                ?>
                                                <tr style="text-align: left;">
                                                    <td><?php echo ucfirst($item_name);?></td>
                                                    <td><?php echo ucfirst($total_quantity);?></td>
                                                    <td><?php echo ucfirst($total_price);?></td>
                                                </tr>
                                                <?php

                                            }
                                            ////////////////////////////// while fetched end
                                        } else {
                                            ?>
                                            <tr style="text-align: left;">
                                                <td>No Sales</td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        </table>
                                    </div>
                                    <br>
                                    <!------------------------------>
                                    <!------------------------------>
                                    <!------------------------------>
                                    <?php
                                    $sold_fetched = mysqli_query($con, "SELECT item_id, SUM(quantity) AS sold_sum FROM item_sold $where GROUP BY item_id ORDER BY SUM(quantity) ASC LIMIT 5 ");
                                        $capture_num_row = mysqli_num_rows($sold_fetched);
                                        ?>
                                    <div class="table-responsive" style="max-height: 400px;">
                                        <table class="table table-responsive-sm-5 mb-0" style="">
                                            <header><strong>TOP 5 SELDOM SELLING EQUIPMENT</strong></header>
                                            <thead>
                                                <tr style="text-align: left;">
                                                    <td><strong>Name</strong></td>
                                                    <td><strong>Quantity</strong></td>
                                                    <td><strong>Sales</strong></td>
                                                </tr>
                                            </thead>
                                        <?php
                                        if ($capture_num_row>0) {
                                            ////////////////////////////// while fetched start
                                            while ($sold_fetched_row = mysqli_fetch_assoc($sold_fetched)) {
                                                $total_quantity = $sold_fetched_row['sold_sum'];
                                                $item_id_fetched = $sold_fetched_row['item_id'];

                                                $item_info_fetched = mysqli_query($con, "SELECT * FROM equipments WHERE id ='$item_id_fetched' ");
                                                $item_info_fetched_row = mysqli_fetch_assoc($item_info_fetched);
                                                $item_name = $item_info_fetched_row['equip_name'];
                                                $item_price = $item_info_fetched_row['price'];

                                                $total_price = $total_quantity*$item_price;
                                                ?>
                                                <tr style="text-align: left;">
                                                    <td><?php echo ucfirst($item_name);?></td>
                                                    <td><?php echo ucfirst($total_quantity);?></td>
                                                    <td><?php echo ucfirst($total_price);?></td>
                                                </tr>
                                                <?php

                                            }
                                            ////////////////////////////// while fetched end
                                        } else {
                                            ?>
                                            <tr style="text-align: left;">
                                                <td>No Sales</td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        </table>
                                    </div>
                                    <!------------------------------>
                                    <!------------------------------>
                                    <!------------------------------>
                                </div>
                                <br>
                            </div>
                            <div class="col-sm-5">
                            <!------------------------------------------------------->
                            <!------------------------------------------------------->
                                <div class="col-sm-10" style="margin: auto; font-size: 12px; text-align: left; padding: 10px; border-radius: 5px; background-color:rgba(0, 15, 13, 0.04); margin: auto;">
                                        <div class="table-responsive" style="max-height: 400px;">
                                            <table class="table table-responsive-sm-5 mb-0" style="">
                                                <header><strong>TOP 5 BEST SELLING PRODUCT</strong></header>
                                                <thead>
                                                    <tr style="text-align: left;">
                                                        <td><strong>Name</strong></td>
                                                        <td><strong>Quantity</strong></td>
                                                        <td><strong>Sales</strong></td>
                                                    </tr>
                                                </thead>
                                            <?php
                                            $sold_fetched = mysqli_query($con, "SELECT item_id, SUM(quantity) AS sold_sum FROM product_sold $where GROUP BY item_id ORDER BY SUM(quantity) DESC LIMIT 5 ");
                                            $capture_num_row = mysqli_num_rows($sold_fetched);
                                            if ($capture_num_row>0) {
                                                ////////////////////////////// while fetched start
                                                while ($sold_fetched_row = mysqli_fetch_assoc($sold_fetched)) {
                                                    $total_quantity = $sold_fetched_row['sold_sum'];
                                                    $item_id_fetched = $sold_fetched_row['item_id'];

                                                    $item_info_fetched = mysqli_query($con, "SELECT * FROM products WHERE id ='$item_id_fetched' ");
                                                    $item_info_fetched_row = mysqli_fetch_assoc($item_info_fetched);
                                                    $item_name = $item_info_fetched_row['product_name'];
                                                    $item_price = $item_info_fetched_row['price'];

                                                    $total_price = $total_quantity*$item_price;
                                                    ?>
                                                    <tr style="text-align: left;">
                                                        <td><?php echo ucfirst($item_name);?></td>
                                                        <td><?php echo ucfirst($total_quantity);?></td>
                                                        <td><?php echo ucfirst($total_price);?></td>
                                                    </tr>
                                                    <?php

                                                }
                                                ////////////////////////////// while fetched end
                                            } else {
                                                ?>
                                                <tr style="text-align: left;">
                                                    <td>No Sales</td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                            </table>
                                        </div>
                                        <?php
                                        //////////////////////////////// fetched end
                                    ?>
                                    <br>
                                </div>
                                <br>
                                <!------------------------------------------------------->
                                <!------------------------------------------------------->
                                <div class="col-sm-10" style="margin: auto; font-size: 12px; text-align: left; padding: 10px; border-radius: 5px; background-color: rgba(0, 15, 13, 0.04); margin: auto;">
                                    <?php
                                        /////////////////////////////// fetched
                                        $sold_fetched = mysqli_query($con, "SELECT item_id, SUM(quantity) AS sold_sum FROM item_sold $where GROUP BY item_id ORDER BY SUM(quantity) DESC LIMIT 5 ");
                                        $capture_num_row = mysqli_num_rows($sold_fetched);
                                        ?>
                                        <div class="table-responsive" style="max-height: 400px;">
                                            <table class="table table-responsive-sm-5 mb-0" style="">
                                                <header><strong>TOP 5 BEST SELLING EQUIPMENT</strong></header>
                                                <thead>
                                                    <tr style="text-align: left;">
                                                        <td><strong>Name</strong></td>
                                                        <td><strong>Quantity</strong></td>
                                                        <td><strong>Sales</strong></td>
                                                    </tr>
                                                </thead>
                                            <?php
                                            if ($capture_num_row>0) {
                                                ////////////////////////////// while fetched start
                                                while ($sold_fetched_row = mysqli_fetch_assoc($sold_fetched)) {
                                                    $total_quantity = $sold_fetched_row['sold_sum'];
                                                    $item_id_fetched = $sold_fetched_row['item_id'];

                                                    $item_info_fetched = mysqli_query($con, "SELECT * FROM equipments WHERE id ='$item_id_fetched' ");
                                                    $item_info_fetched_row = mysqli_fetch_assoc($item_info_fetched);
                                                    $item_name = $item_info_fetched_row['equip_name'];
                                                    $item_price = $item_info_fetched_row['price'];

                                                    $total_price = $total_quantity*$item_price;
                                                    ?>
                                                    <tr style="text-align: left;">
                                                        <td><?php echo ucfirst($item_name);?></td>
                                                        <td><?php echo ucfirst($total_quantity);?></td>
                                                        <td><?php echo ucfirst($total_price);?></td>
                                                    </tr>
                                                    <?php

                                                }
                                                ////////////////////////////// while fetched end
                                            } else {
                                                ?>
                                                <tr style="text-align: left;">
                                                    <td>No Sales</td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                            </table>
                                        </div>
                                        <?php
                                        //////////////////////////////// fetched end
                                    ?>
                                </div>
                                <br>
                            </div>
                            <!------------------------------------------------------->
                            <!------------------------------------------------------->
                        </div>
                        <!------------------------------------------------------->
                        <!------------------------------------------------------->
                        <!------------------------------------------------------->
                    </div>
                </div>
                <!------------------------------------------------------->
                <!------------------------------------------------------->
            </div>
        </div>
        <br>
        <br>
        <?php
    }
else
    {
        header("location: index.php");
    }
?>
</body>
</html>