<?php
error_reporting(1);
date_default_timezone_set('Asia/Phnom_Penh');
include("connection.php");

use PHPMailer\PHPMailer\PHPMailer; 
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';

$mail = new PHPMailer(true);

$password = $_GET['access_token'];
$login_id = $_GET['user_access_verification_number'];

$emailuser = mysqli_query($con, "SELECT * FROM login WHERE id = '$login_id'") or die ("token already expired or used");
$row = mysqli_fetch_assoc($emailuser);
$email = $row['email'];
$password2 = $row['password'];
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Cloud Based Interbranch Poultry Sales Information System</title>
	<link href="design/css/design.css" rel="stylesheet">
	<title>Create new password</title>
</head>
<body>
<body class="h-100" data-typography="roboto" data-theme-version="light" data-layout="vertical" data-nav-headerbg="color_3" data-headerbg="color_1" data-sidebar-style="mini" data-sibebarbg="color_3" data-sidebar-position="fixed" data-header-position="fixed" data-container="wide" direction="ltr" data-primary="color_1" style="background-image: url('design/a.png') !important">
    <div class="authincation h-100 mt-5 pt-5">
        <div class="container h-100">
            <div class="row justify-content-center h-100 align-items-center">
                <div class="col-md-6">
                    <div class="authincation-content" style="box-shadow: 3px 3px 8px #b1b1b1, -3px -3px 8px #ffffff;">
                        <div class="row no-gutters">
                            <div class="col-xl-12">
                                <div class="auth-form">
                                    <h4 class="text-center mb-4">Create New Password</h4>
                                    <form id="login" action="" method="post">
                                        <div class="form-group">
                                            <label class="mb-1"><strong>Enter New Password</strong></label>
                                            <input input id="password1" type="password" name="new_password" class="form-control" style="margin-bottom: 20px;border-radius: 25px;box-shadow: inset 8px 8px 8px #cbced1, inset -8px -8px 8px #ffffff;" placeholder="Enter New Password" required>
                                        </div>
                                        <div class="form-group">
                                            <label class="mb-1"><strong>Confirm New Password</strong></label>
                                            <input id="password2" type="password" name="confirm_password" class="form-control" style="margin-bottom: 20px;border-radius: 25px;box-shadow: inset 8px 8px 8px #cbced1, inset -8px -8px 8px #ffffff;"  placeholder="Confirm New Password" required>
                                        </div>
                                        <input type="checkbox" onclick="showPassword()">Show Password
																				<br>
                                        <?php
                                            if(isset($_POST['change_pass_submit'])){

                                                $confirm_password = md5($_POST['confirm_password']);
                                                $new_password = md5($_POST['new_password']);
                                                if ($new_password==$confirm_password) {
                                                    mysqli_query($con, "UPDATE `login` SET `password`='$new_password' WHERE id = '$login_id'" ) or die ("token already expired or used");

                                                    try {
                                                        $mail->isSMTP();                                   
                                                        $mail->Host = 'smtp.gmail.com';  
                                                        $mail->SMTPAuth = true;                             
                                                        $mail->Username = 'cbipsis2022@gmail.com';            
                                                        $mail->Password = 'cbipsis1234';                     
                                                        $mail->SMTPSecure = 'tls';                         
                                                        $mail->Port = 587;     

                                                        $mail->setFrom('cbipsis2022@gmail.com', "Cloud Based Interbranch Poultry Sales Information System");
                                                        $mail->addAddress($email, "Receiver");

                                                        $mail->Subject = 'Change Password Notification';
                                                        $mail->Body    = 'your password has been successfully changed, confirm this, it was you';

                                                        $mail->send();
                                                    
                                                    } catch (Exception $e) { // handle error.
                                                        echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
                                                    }

                                                    header("location: index.php?text=Change password successfully");

                                                } else {
                                                    echo "<p style='color:red;'>Confirm password did not match";
                                                }
                                            }
                                        ?>
																				<br>
                                        <div class="text-center">
                                            <input id='submit' type="submit" name="change_pass_submit" class="btn btn-primary btn-block" style="background-color: #787312; border-color: #a1990b; box-shadow: 3px 3px 8px #b1b1b1, -3px -3px 8px #ffffff; " value="Confirm">
                                        </div>
                                    </form>
                                    <div class="new-account mt-3">
                                        <p>Already have an account? <a class="text-primary" href="index.php">Log in now</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script src="vendor/global/global.min.js"></script>
<script src="vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
<script src="js/custom.min.js"></script>
<script src="js/deznav-init.js"></script>
<script>
function showPassword() {
  var x = document.getElementById("password1");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
  var x = document.getElementById("password2");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
</script>
</body>
</html>