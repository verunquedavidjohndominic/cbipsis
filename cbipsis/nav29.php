<?php session_start();
error_reporting(1);
date_default_timezone_set('Asia/Manila');
include("connection.php");
$date = ucfirst(date('F j, Y h:i:s'));

$page_validator = $_SESSION['valid'];
$class_checker = mysqli_query($con, "SELECT * FROM login WHERE id = '$page_validator'") or die("error logging in");
$row = mysqli_fetch_assoc($class_checker);
$class = $row['class'];
$location = $row['location'];
$branch = $row['branch'];
$price_list = mysqli_query($con, "SELECT * FROM equipments");
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<?php

$tools = mysqli_query($con, "SELECT * FROM farmtool");
if(isset($_SESSION['valid']))
    {
        ////////important per page
        if ($class==0) {
           ?>
                <meta http-equiv="refresh" content="0;url=dashboard.php?dash=nav5" />
           <?php
        }
        ////////
if (isset($_POST['add_tool'])) {
    $name = $_POST['name'];
    $description = $_POST['description'];

    mysqli_query($con, "INSERT INTO `farmtool`(`name`, `description`) VALUES ('$name','$description')") or die("<script>alert('$name tool is already exist')</script><meta http-equiv='refresh' content='0;url=dashboard.php?dash=nav29' />");
    echo "<script>alert('".$name." is successfully added to the system')</script>";
}
if (isset($_POST['rec_tool'])) {
    $name = $_POST['name'];
    $quantity = $_POST['quantity'];
    mysqli_query($con, "INSERT INTO `farmtool_stock`(`quantity`, `tool_id`) VALUES ('$quantity','$name')");
    echo "<script>alert('tools successfully receive')</script>";
}
if (isset($_POST['use_tool'])) {
$branch_no = $_POST['used_tool'];
$used_tool_total_quantity = $_POST['used_tool_total_quantity'];
$branch_number = mysqli_query($con, "SELECT * FROM farmtool WHERE id = '$branch_no'");
$branch_number_row = mysqli_fetch_assoc($branch_number);
$branch_number_number = $branch_number_row['name'];
?>
    <form class="popup" method="post">
        <div class="popup-content">
        <head>Current Tool ( <?php echo $branch_number_number;?> )</head>
        <br>
        <br>
        <input type="text" name="quantity" autofocus="" style="text-align: center;" placeholder="Enter Quantity" pattern="[0-9]{0,100}">
        <br>
        <br>
        <input type="text" name="condition" autofocus="" style="text-align: center;" placeholder="Enter Condition">
        <input type="hidden" name="getter_used_tool_id" value="<?php echo $branch_no;?>">
        <input type="hidden" name="used_tool_total_quantity" value="<?php echo $used_tool_total_quantity;?>">
        <br>
        <br>
        <button name="used_tool_confirm">Use?</button>
        <button name="">Cancel</button>
        </div>
    </form>
<?php
}
if (isset($_POST['used_tool_confirm'])) {
    $getter_used_tool_id = $_POST['getter_used_tool_id'];
    $used_tool_total_quantity = $_POST['used_tool_total_quantity'];
    $quantity = $_POST['quantity'];
    $condition = $_POST['condition'];
    if ($used_tool_total_quantity>=$quantity) {
         mysqli_query($con, "INSERT INTO `farmtool_stock`(`quantity`, `tool_id`) VALUES ('-$quantity','$getter_used_tool_id')");

         mysqli_query($con, "INSERT INTO `used_tool`(`tool_id`, `condition`, `incharge`, `quantity`) VALUES ('$getter_used_tool_id','$condition','$page_validator','$quantity')");
         echo "<script>alert('tools is listed as used')</script>";
    } else {
        echo "<script>alert('tools stock is only ".$used_tool_total_quantity."')</script>";
    }
   


}
?>
<div class="col-sm-11" style="margin: auto; background-color: white; padding: 25px; border-radius: 5px;">
    <div class="row">
        <div class="col-sm-12" style=" margin: auto;">
            <form id="" method="POST" style="background-color: white; padding: 25px; border-radius: 5px;">
                <div class="col-sm">
                    <label class="mb-1"><strong>ADD FARM TOOLS</strong></label>
                    <br>
                    <div class="row">
                        <div class="col-sm">
                            <label class="mb-1"><strong>Name</strong></label>
                            <input type="text" class="form-control" name="name" placeholder="Enter Product Name" required="" style="text-align: center;">
                            <br>
                        </div>
                        <div class="col-sm">
                            <label class="mb-1"><strong>Description</strong></label>
                            <input type="text" class="form-control" name="description" placeholder="Enter description" required="" style="text-align: center;">
                            <br>
                        </div>
                        <div class="col-sm">
                            <div class="form-group">
                                <label class="mb-1"><strong>&nbsp</strong></label>
                                <button type="submit" id="submit" name="add_tool" class="btn btn-primary btn-block" style="background-color: hsla(21, 51%, 17%, 1); border-color: white; width: 150px; margin: auto; ">Save</button>
                                <br>
                            </div>
                        </div>
                        <div class="col-sm">
                        </div>
                    </div>
                </div>
            </form>
            <form id="" method="POST" style="background-color: white; padding: 25px; border-radius: 5px;">
                <div class="col-sm">
                    <label class="mb-1"><strong>RECEIVE TOOLS STOCK</strong></label>
                    <br>
                    <div class="row">
                        <div class="col-sm">
                            <label class="mb-1"><strong>Name</strong></label>
                            <select name = "name" class="form-control">
                            <?php 
                               while($category_list_row = mysqli_fetch_array($tools)):;
                            ?>
                                <option value="<?php echo $category_list_row[0];?>">
                                <?php echo ucfirst($category_list_row[1]);?>
                                </option>
                            <?php 
                                endwhile;
                            ?>
                            </select>
                            <br>
                        </div>
                        <div class="col-sm">
                            <label class="mb-1"><strong>Quantity</strong></label>
                            <input type="text" class="form-control" name="quantity" placeholder="Enter Quantity" required="" style="text-align: center;">
                            <br>
                        </div>
                        <div class="col-sm">
                            <div class="form-group">
                                <label class="mb-1"><strong>&nbsp</strong></label>
                                <button type="submit" id="submit" name="rec_tool" class="btn btn-primary btn-block" style="background-color: hsla(21, 51%, 17%, 1); border-color: white; width: 150px; margin: auto; ">Confirm</button>
                                <br>
                            </div>
                        </div>
                        <div class="col-sm">
                        </div>
                    </div>
                </div>
            </form>
            <div class="col-sm-11" style="background-color: white; padding: 20px; border-radius: 5px; margin: auto;">
                <div class="table-responsive" style="max-height: 300px;">
                    <header><strong>FARM TOOLS </strong></header>
                        <table class="table table-responsve-sm-5 mb-0" style="">
                            <thead>
                                <tr>
                                    <th><strong>NAME</strong></th>
                                    <th><strong>DESCRIPTION</strong></th>
                                    <th><strong>QUANTITY</strong></th>
                                    <th><strong></strong></th>
                                </tr>
                            </thead>
                            <?php
                                $tools_fetch = mysqli_query($con, "SELECT DISTINCT id FROM farmtool");
                                while ($tools_fetch_row = mysqli_fetch_assoc($tools_fetch)) {
                                    ?>
                                    <form method="post">
                                    <?php
                                    $id = $tools_fetch_row['id'];

                                    $tools_fetch_fetch = mysqli_query($con, "SELECT DISTINCT tool_id, SUM(quantity) AS quan_sum FROM farmtool_stock WHERE tool_id = '$id'");
                                    $tools_fetch_fetch_row = mysqli_fetch_assoc($tools_fetch_fetch);
                                    $sum = $tools_fetch_fetch_row['quan_sum'];

                                    $fetch_name_name = mysqli_query($con, "SELECT * FROM farmtool WHERE id = '$id'");
                                    $fetch_name_name_row = mysqli_fetch_assoc($fetch_name_name);
                                    $id_id = $fetch_name_name_row['id'];
                                    $name_name = $fetch_name_name_row['name'];
                                    $desc_desc = $fetch_name_name_row['description'];

                                    ?>
                                    <tr>
                                        <td><?php echo $name_name; ?></td>
                                        <td><?php echo $desc_desc; ?></td>
                                        <td><?php echo $sum; ?></td>
                                        <input type="hidden" name="used_tool_total_quantity" value="<?php echo $sum;?>">
                                        <input type="hidden" name="used_tool" value="<?php echo $id_id?>">
                                        <td><button type="submit" id="submit" name="use_tool" class="btn btn-primary btn-block" style="background-color: hsla(21, 51%, 17%, 1); border-color: white; width: 100px; margin: auto; ">Use</button></td>
                                    </tr>
                                    </form>
                                    <?php
                                }
                            ?>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                        </table>                      
                </div>
                <!--------------------------------------------------------------->
            </div>
            <div class="col-sm-11" style="background-color: white; padding: 20px; border-radius: 5px; margin: auto;">
                <div class="table-responsive" style="max-height: 300px;">
                    <header><strong>FARM USED TOOLS </strong></header>
                        <table class="table table-responsve-sm-5 mb-0" style="">
                            <thead>
                                <tr>
                                    <th><strong>NAME</strong></th>
                                    <th><strong>QUANTITY</strong></th>
                                    <th><strong>CONDITION</strong></th>
                                    <th><strong>INCHARGE</strong></th>
                                </tr>
                            </thead>
                            <?php
                                $tools_fetch_fetch = mysqli_query($con, "SELECT * FROM used_tool ORDER BY id desc");
                                while ($tools_fetch_fetch_row = mysqli_fetch_assoc($tools_fetch_fetch)) {
                                    ?>
                                    <form method="post">
                                    <?php
                                    $id = $tools_fetch_fetch_row['tool_id'];
                                    $sum = $tools_fetch_fetch_row['quantity'];
                                    $condition = $tools_fetch_fetch_row['condition'];
                                    $incharge = $tools_fetch_fetch_row['incharge'];

                                    $fetch_name_name = mysqli_query($con, "SELECT * FROM farmtool WHERE id = '$id'");
                                    $fetch_name_name_row = mysqli_fetch_assoc($fetch_name_name);
                                    $name_name = $fetch_name_name_row['name'];

                                    $incharge_get = mysqli_query($con, "SELECT * FROM login WHERE id = '$incharge'");
                                    $incharge_get_row = mysqli_fetch_assoc($incharge_get);
                                    $lastname = $incharge_get_row['lname'];
                                    $name = $incharge_get_row['name'];

                                    ?>
                                    <tr>
                                        <td><?php echo $name_name; ?></td>
                                        <td><?php echo $desc_desc; ?></td>
                                        <td><?php echo $condition; ?></td>
                                        <td><?php echo $name," ",$lastname; ?></td>
                                    </tr>
                                    </form>
                                    <?php
                                }
                            ?>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                        </table>                      
                </div>
                <!--------------------------------------------------------------->
            </div>
        </div>
    </div>
</div>

<?php
    }
else
    {
        header("location: index.php");
    }
?>
<style type="text/css">
    .popup {
        position: absolute;
        z-index: 1;
        width: 75%;
        text-align: center;
    }
    .popup-content {
        background-color: white;
        box-shadow: 1px 1px 1px 1px gray,1px 1px 1px 1px gray;
        width: 300px;
        text-align: center;
        padding: 20px;
        border-radius: 10px;
        margin: auto;
        margin-top: 150px;
    }

    .popup-content input {
        border-radius: 5px;
    }

    .popup-content button {
        background-color: hsla(21, 51%, 17%, 1);
        color: white;
        border-radius: 10px;
        width: 80px;
        border:none;
    }
    .popup-content button:hover {
        box-shadow: 1px 1px 2px 2px brown,1px 1px 2px 2px brown;
    }
    @media screen and (max-width: 750px)
        {
            .popup-content {
                background-color: white;
                box-shadow: 1px 1px 1px 1px gray,1px 1px 1px 1px gray;
                margin-left: 15px;
            }
        }
</style>
</body>
</html>