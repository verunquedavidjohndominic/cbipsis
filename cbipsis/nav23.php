<?php session_start();
error_reporting(1);
date_default_timezone_set('Asia/Manila');
include("connection.php");
$date = ucfirst(date('Y-m-d'));
$date2 = strval(date('mdyhis'));
$page_validator = $_SESSION['valid'];
$class_checker = mysqli_query($con, "SELECT * FROM login WHERE id = '$page_validator'") or die("error logging in");
$row = mysqli_fetch_assoc($class_checker);
$class = $row['class'];
$location = $row['location'];
$branch = $row['branch'];
$bn_list = mysqli_query($con, "SELECT * FROM branch");
$bn_no = mysqli_query($con, "SELECT * FROM branch_no");
$getid = $_GET['id'];
$getloc = $_GET['loc'];
$getbranch = $_GET['branch'];
$box_name_fetch = mysqli_query($con, "SELECT * FROM products WHERE id = '$getid'");
$box_name_row = mysqli_fetch_assoc($box_name_fetch);
$box_name = strtoupper($box_name_row['product_name']);
///////////////////////////////////////////////////////
$cart_list_sql7 = mysqli_query($con, "SELECT DISTINCT item_id, type FROM cart WHERE employee_id = '$page_validator' ORDER BY type");
while ($cart_list_sql_row7 = mysqli_fetch_assoc($cart_list_sql7)) {
    $cart_item_id7 = $cart_list_sql_row7['item_id'];
    $cart_type7 = $cart_list_sql_row7['type'];

    if ($cart_type7==0) {
        $product_fetch7 = mysqli_query($con, "SELECT * FROM products WHERE id = '$cart_item_id7' ");
        $product_fetch_row7 = mysqli_fetch_assoc($product_fetch7);
        $item_name7 = $product_fetch_row7['product_name'];
        $item_price7 = $product_fetch_row7['price'];

        $cart_list7 = mysqli_query($con, "SELECT SUM(quantity) AS value_sum3 FROM cart WHERE item_id ='$cart_item_id7' AND location = '$location' AND branch_no = '$branch' AND type = 0 ");
        $cart_list_row7 = mysqli_fetch_assoc($cart_list7);
        $cart_quantity7 = $cart_list_row7['value_sum3'];
        $type_type7 = "Product";

    } else {
        $product_fetch7 = mysqli_query($con, "SELECT * FROM equipments WHERE id = '$cart_item_id7' ");
        $product_fetch_row7 = mysqli_fetch_assoc($product_fetch7);
        $item_name7 = $product_fetch_row7['equip_name'];
        $item_price7 = $product_fetch_row7['price'];

        $cart_list7 = mysqli_query($con, "SELECT SUM(quantity) AS value_sum3 FROM cart WHERE item_id ='$cart_item_id7' AND location = '$location' AND branch_no = '$branch' AND type = 1 ");
        $cart_list_row7 = mysqli_fetch_assoc($cart_list7);
        $cart_quantity7 = $cart_list_row7['value_sum3'];
        $type_type7 = "Equipment";
    }
    $total_price_price7 = $cart_quantity7*$item_price7;
    $total_total7 += $total_price_price7;          
}
///////////////////////////////////////////////////////
$chart_checkered = mysqli_query($con, "SELECT quantity FROM cart Where employee_id = '$page_validator'");
$chart_checkered_row2 = mysqli_num_rows($chart_checkered);
while ($chart_checkered_row = mysqli_fetch_assoc($chart_checkered)) {
    $quantity += $chart_checkered_row['quantity'];
}
if ($quantity<=0) {
    echo "<script>alert('Cart is empty');</script>";
    ?><meta http-equiv="refresh" content="0;url=dashboard.php?dash=nav6"/><?php
}

?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<?php   
if (isset($_POST['confirm_order'])) {
    $name1 = ucfirst($_POST['name']);
    $lname1 = ucfirst($_POST['lname']);
    $ml1 = ucfirst($_POST['ml']);
    $extention1 = ucfirst($_POST['ext']);
    $contact1 = $_POST['phone'];
    $address1 = ucfirst($_POST['address']);
    $cash_rec1 = $_POST['gcash'];
    $cash_rec2 = $_POST['cash'];
    /////////////////////////////////////////////////////////////////////
    $cart_list_sql = mysqli_query($con, "SELECT DISTINCT item_id, type FROM cart WHERE employee_id = '$page_validator' ORDER BY type");
    $cart_list_sql2 = mysqli_query($con, "SELECT DISTINCT SUM(id) AS value_sumid FROM cart WHERE employee_id = '$page_validator' ORDER BY type");
    $cart_list_sql_row2 = mysqli_fetch_assoc($cart_list_sql2);
    $id_sum_sold_no = $cart_list_sql_row2['value_sumid'];
    ///////////////////////
    if ($name1=="") {
        echo "<script>alert('Name is required');</script>";
    } else {
        if ($lname1=="") {
             echo "<script>alert('Last Name is required');</script>";
        } else {
            if ($address1=="") {
                echo "<script>alert('Address is required');</script>";
            } else {
                if ($contact1=="") {
                    echo "<script>alert('Contact Number is required');</script>";
                } else {
                    if ($real_cus_price="") {
                        echo "<script>alert('Required to fill up Payment method');</script>";
                    } else {

                       if ($cash_rec1==""&&$cash_rec2=="") {
                           echo "<script>alert('Required to fill up Payment method');</script>";
                       } else {
                             //////////////////////////////////////////////////////////////////////
                            while ($cart_list_sql_row = mysqli_fetch_assoc($cart_list_sql)) {
                                $cart_item_id = $cart_list_sql_row['item_id'];
                                $cart_type = $cart_list_sql_row['type'];

                                if ($cart_type==0) {
                                    $product_fetch = mysqli_query($con, "SELECT * FROM products WHERE id = '$cart_item_id' ");
                                    $product_fetch_row = mysqli_fetch_assoc($product_fetch);
                                    $item_name = $product_fetch_row['product_name'];
                                    $item_price = $product_fetch_row['price'];

                                    $cart_list = mysqli_query($con, "SELECT SUM(quantity) AS value_sum3 FROM cart WHERE item_id ='$cart_item_id' AND location = '$location' AND branch_no = '$branch' AND type = 0 ");
                                    $cart_list_row = mysqli_fetch_assoc($cart_list);
                                    $cart_quantity = $cart_list_row['value_sum3'];
                                    $type_type = "Product";

                                } else {
                                    $product_fetch = mysqli_query($con, "SELECT * FROM equipments WHERE id = '$cart_item_id' ");
                                    $product_fetch_row = mysqli_fetch_assoc($product_fetch);
                                    $item_name = $product_fetch_row['equip_name'];
                                    $item_price = $product_fetch_row['price'];

                                    $cart_list = mysqli_query($con, "SELECT SUM(quantity) AS value_sum3 FROM cart WHERE item_id ='$cart_item_id' AND location = '$location' AND branch_no = '$branch' AND type = 1 ");
                                    $cart_list_row = mysqli_fetch_assoc($cart_list);
                                    $cart_quantity = $cart_list_row['value_sum3'];
                                    $type_type = "Equipment";
                                }
                                $total_price_price = $cart_quantity*$item_price;
                                $total_total += $total_price_price;
                                ////////////////////////////
                                if ($total_total==0) {
                                    echo "<script>alert('Cart is empty');</script>";
                                    ?><meta http-equiv="refresh" content="0;url=dashboard.php?dash=nav6" /><?php
                                }
                                ////////////////////////////
                                if ($cart_type==0) {
                                    $insert_into = "product_sold";
                                } else {
                                    $insert_into = "item_sold";
                                }
                                ////////////////////////////
                                if ($cash_rec2=="") {
                                    $real_cus_price = $total_total;
                                } else {
                                    $real_cus_price = $cash_rec2;
                                }
                                ////////////////////////////////
                                if ($total_total>$real_cus_price) {
                                    
                                } else {
                                    $tosold = mysqli_query($con, "INSERT INTO `$insert_into`(`quantity`, `date`, `location`, `branch`, `item_id`, `employee`, `sold_no`, `total_rec`) VALUES ('$cart_quantity','$date','$location','$branch','$cart_item_id','$page_validator','$date2$id_sum_sold_no','$total_price_price')");
                                }          
                            }
                            //////////////////////////////////////////// end while
                            if ($total_total>$real_cus_price) {
                                    echo "<script>alert('Cash is lower than the selling price');</script>";
                                } else {
                                    ////////////////////////////////////////////
                                    $cart_list_sql20 = mysqli_query($con, "SELECT DISTINCT SUM(id) AS value_sumid FROM cart WHERE employee_id = '$page_validator' ORDER BY type");
                                    $cart_list_sql_row20 = mysqli_fetch_assoc($cart_list_sql2);
                                    $id_sum_sold_no2 = $cart_list_sql_row20['value_sumid'];
                                    ///////////////////////////////////////////
                                    mysqli_query($con, "INSERT INTO `transaction`(`employee_id`, `date`, `sold_no`) VALUES ('$page_validator','$date','$date2$id_sum_sold_no')");
                                    mysqli_query($con, "INSERT INTO `costumer`(`name`, `surename`, `ml`, `extention`, `contact`, `employee_id`, `cash_rec`, `sold_no`, `address`) VALUES ('$name1','$lname1','$ml1','$extention1','$contact1','$page_validator','$real_cus_price','$date2$id_sum_sold_no','$address1')");
                                    //////////////////////////////////////////
                                    mysqli_query($con, "DELETE FROM `cart` WHERE employee_id = '$page_validator'");
                                     ?><meta http-equiv="refresh" content="0;url=dashboard.php?dash=nav24&sold_no=<?php echo $date2,$id_sum_sold_no?>"/><?php
                                }
                             ///////////////////////// ending
                       }
                    }
                }
            }
        }
    }


}

if(isset($_SESSION['valid']))
    {
        ////////important per page
     
        ////////
        ?>
        <style type="text/css"> #nav6,#nav-5-1 {color: white; font-weight: bold; background-color:gray;} #nav-5-1{padding: 8px; border-radius: 2px; position: static;} #nav6:hover,#nav-5-1:hover { color: white; background-color: black; text-shadow: none;}</style>
        <?php
        /////////
     	if ($getloc=="") {
     		$header_info = "";
     	} else {
     		$header_info = strtoupper($getloc);
     	}
		?>
		<div class="row">
        <div class="col-sm"></div>
        <div class="col-sm-11" style="margin: auto;">
        <div id="add_stock"  style="background-color: white; padding: 25px; border-radius: 5px;">
        <label class="mb-1"><strong>CUSTOMER INFO</strong></label>
        <div style="text-align: right;">
        <form method="post" id="order_form">
            <button type="submit" name="confirm_order" style="background-color: white; border-radius: 5em; border-color: white; box-shadow: ; width: 50px; margin: auto; "><i class="submit fa fa-truck" aria-hidden="true" style="font-size: 30px;"></i></button>
            <button type="submit" formaction="dashboard.php?dash=nav22"  style="background-color: white; border-radius: 5em; border-color: white; box-shadow: ; width: 50px; margin: auto; "><i class="fa fa-arrow-left" aria-hidden="true" style="font-size: 30px;"></i></button>
        <br>
        <div class="row">
        <div class="col-sm-12" style="text-align: left;">
            <!-------------------------------------------------------->
            <strong><header style=" font-weight: bold; padding-top: 2px;">TOTAL BILL: ₱ <?php echo ($total_total7);?></header></strong>
            <br>
            
            <!-------------------------------------------------------->
            <!-------------------------------------------------------->
            <!-------------------------------------------------------->
            <!-------------------------------------------------------->
            <!-------------------------------------------------------->
            <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="mb-1"><strong>Name</strong></label>
                    <input type="text" class="form-control" name="name" placeholder="Enter Name" style="text-align: center; text-transform: capitalize;">
                </div>
                <div class="form-group">
                    <label class="mb-1"><strong>Middle Name</strong></label>
                    <input type="text" class="form-control" name="ml" placeholder="Enter Middle Name" style="text-align: center; text-transform: capitalize;">
                </div>
                <div class="form-group">
                    <label class="mb-1"><strong>Lastname</strong></label>
                    <input type="text" class="form-control" name="lname" placeholder="Enter Last Name" style="text-align: center; text-transform: capitalize;">
                </div>
                <div class="form-group">
                    <label class="mb-1"><strong>Name Ext.</strong></label>
                    <input type="text" class="form-control" name="ext" placeholder="Enter Name Ext." style="text-align: center; text-transform: capitalize;">
                </div>
            </div>
            <!-------------------------------------------------------->
            <!-------------------------------------------------------->
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="mb-1"><strong>Phone</strong></label>
                    <input type="text" class="form-control" name="phone" placeholder="Enter Username" pattern="[0-9]{11,11}" style="text-align: center;" maxlength="11">
                </div>
                <div class="form-group">
                    <label class="mb-1"><strong>Address</strong></label>
                    <input type="text" class="form-control" name="address" placeholder="Enter Username" style="text-align: center; text-transform: capitalize;">
                    <input type="hidden" class="form-control" name="real_price" value="<?php echo($real_price)?>">
                </div>
                <div class="form-group">
                    <div id="button_a" style="text-align: left; position:">
                        <a onclick="myFunction1()" type="submit" style="background-color: white; border-color: white; box-shadow:1px 1px gray ; width: 70px; margin: auto; text-align: center; border-radius: 5px;  font-size: 15px; "><strong>CASH</strong></a>
                        <a onclick="myFunction2()" type="submit"  style="background-color: white; border-color: white; box-shadow:1px 1px gray ; width: 70px; margin: auto; text-align: center; border-radius: 5px;  font-size: 15px; "><strong>GCASH</strong></a>
                       <!-- <a onclick="myFunction3()" type="submit" style="background-color: white; border-color: white; box-shadow:1px 1px gray; width: 70px; margin: auto;
                            text-align: center; border-radius: 5px; font-size: 15px;"><i class="fa fa-credit-card" aria-hidden="true"></i></a>-->
                    </div>
                    <br>
                    <input id="myDIV1" type="text" class="form-control" name="cash" placeholder="Enter Cash Amount" pattern="[0-9]{0,10}" style="text-align: center;">
                    <!-- GCASH API https://docs.adyen.com/payment-methods/gcash/api-only this the link for the info need API key and Auth for GLOBE API to use this content -->
                    <input id="myDIV2" type="text" class="form-control" name="gcash" placeholder="Enter GCASH Number" pattern="[0-9]{11,11}" maxlength="11" style="display: none; text-align: center;">
                   <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
                    <input id="myDIV3" class="creditCardText form-control" maxlength="19" type="text" class="form-control" name="username" placeholder="Enter credit" style="display: none; text-align: center;"> -->
                </div>
                <style type="text/css">
                    #button_a a:hover {
                        background-color: black;
                        color: black;
                    }
                </style>
            </div>
            </div>
            <script>
            function myFunction1() {
              var x = document.getElementById("myDIV1");
              if (x.style.display === "block") {
                x.style.display = "block";
              } else {
                x.style.display = "block";
              }
              var x = document.getElementById("myDIV2");
              if (x.style.display === "none") {
                x.style.display = "none";
              } else {
                x.style.display = "none";
              }
            //  var x = document.getElementById("myDIV3");
            //  if (x.style.display === "none") {
            //    x.style.display = "none";
             // } else {
            //    x.style.display = "none";
              //} //this content is for credit card input
            }
            function myFunction2() {
              var x = document.getElementById("myDIV2");
              if (x.style.display === "block") {
                x.style.display = "block";
              } else {
                x.style.display = "block";
              }
             // var x = document.getElementById("myDIV3");
             // if (x.style.display === "none") {
              //  x.style.display = "none";
             // } else {
             //   x.style.display = "none";
             // } //this content is for credit card input
              var x = document.getElementById("myDIV1");
              if (x.style.display === "none") {
                x.style.display = "none";
              } else {
                x.style.display = "none";
              }
            }
           // function myFunction3() {//this content is for credit card input
             // var x = document.getElementById("myDIV3");//this content is for credit card input
             // if (x.style.display === "block") {
              //  x.style.display = "block";//this content is for credit card input
            //  } else {
             //   x.style.display = "block";//this content is for credit card input
            //  }
            //  var x = document.getElementById("myDIV2");
            //  if (x.style.display === "none") {//this content is for credit card input
           //     x.style.display = "none";
            //  } else {//this content is for credit card input
            //    x.style.display = "none";
            //  }
             // var x = document.getElementById("myDIV1");
            //  if (x.style.display === "none") {//this content is for credit card input
             //   x.style.display = "none";
            //  } else {
             //   x.style.display = "none";//this content is for credit card input
            //  }
           // }
          //  $('.creditCardText').keyup(function() {
           //   var foo = $(this).val().split("-").join(""); // remove hyphens//this content is for credit card input
            //  if (foo.length > 0) {//this content is for credit card input
            //    foo = foo.match(new RegExp('.{1,4}', 'g')).join("-");//this content is for credit card input
             // }//this content is for credit card input
           //   $(this).val(foo);//this content is for credit card input
         //   }); //this content is for credit card input
            </script>
            <!-------------------------------------------------------->
            <!-------------------------------------------------------->
            <!-------------------------------------------------------->
            <!-------------------------------------------------------->
            <!-------------------------------------------------------->
        </div>
        <div class="col-sm"></div>
        </div>
        </form>
        <script type="text/javascript">
            // submit form and get new records kun diin an sender
            $('#order_form').on('.submit', function(event){
             event.preventDefault();
              var form_data = $(this).serialize();
              $.ajax({
               method:"POST",
               data:form_data,
               success:function(data)
               {
                $('#order_form')[0].reset();
                load_unseen_notification();
               }
              });
            });
            // load new notifications
        </script>
        </div>
        <br>
        </div>
        </div>
        <div class="col-sm"></div>
        </div>
		<?php
    }
else
    {
        header("location: index.php");
    }
?>
<br>
<br>
</body>
</html>