<?php session_start();
error_reporting(1);
date_default_timezone_set('Asia/Manila');
include("connection.php");
$date = ucfirst(date('F j, Y'));
$page_validator = $_SESSION['valid'];
$class_checker = mysqli_query($con, "SELECT * FROM login WHERE id = '$page_validator'") or die("error logging in");
$row = mysqli_fetch_assoc($class_checker);
$class = $row['class'];
$location = $row['location'];
$branch = $row['branch'];
$bn_list = mysqli_query($con, "SELECT * FROM branch");
$bn_no = mysqli_query($con, "SELECT * FROM branch_no");
$getid = $_GET['id'];
$getloc = $_GET['loc'];
$getbranch = $_GET['branch'];
$box_name_fetch = mysqli_query($con, "SELECT * FROM products WHERE id = '$getid'");
$box_name_row = mysqli_fetch_assoc($box_name_fetch);
$box_name = strtoupper($box_name_row['product_name']);
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<?php   
if (isset($_POST['refresher'])) {

    ?>
        <meta http-equiv="refresh" content="0;url=dashboard.php?dash=nav6" />
    <?php
}
if (isset($_POST['add_cart'])) {
    $countity = $_POST['countity'];
    $cart_item_id = $_POST['cart_item_id'];
    $cart_item_type = $_POST['cart_item_type'];

    if ($cart_item_type==0) {
       $cart_list_check = mysqli_query($con, "SELECT SUM(quantity) AS value_sum FROM product_sold WHERE item_id ='$cart_item_id' AND location = '$location' AND branch = '$branch' ");
    } else {
        $cart_list_check = mysqli_query($con, "SELECT SUM(quantity) AS value_sum FROM item_sold WHERE item_id ='$cart_item_id' AND location = '$location' AND branch = '$branch' ");
    }
    $cart_list_check_row = mysqli_fetch_assoc($cart_list_check);
    $cart_quantity_check = $cart_list_check_row['value_sum'];
    $cart_quantity_check;
    ////////////////////////////////////////////////
    if ($cart_item_type==0) {
       $cart_list_check2 = mysqli_query($con, "SELECT SUM(quantity) AS value_sum2 FROM product_stock WHERE item_id ='$cart_item_id' AND location = '$location'  AND branch = '$branch' ");
    } else {
        $cart_list_check2 = mysqli_query($con, "SELECT SUM(quantity) AS value_sum2 FROM item_stock WHERE item_id ='$cart_item_id' AND location = '$location'  AND branch = '$branch' ");
    }
    $cart_list_check_row2 = mysqli_fetch_assoc($cart_list_check2);
    $cart_quantity_check2 = $cart_list_check_row2['value_sum2'];
    ///////////////////////////////////////////////
    $cart_list = mysqli_query($con, "SELECT SUM(quantity) AS value_sum3 FROM cart WHERE item_id ='$cart_item_id' AND location = '$location' AND branch_no = '$branch' AND type = '$cart_item_type' ");
    $cart_list_row = mysqli_fetch_assoc($cart_list);
    $cart_quantity = $cart_list_row['value_sum3'];

    $total_stock_count = $cart_quantity_check2-$cart_quantity_check;
    $checker_exe = $total_stock_count-$cart_quantity;

    if ($countity=="") {
         if ($total_stock_count>=$countity) {
            if ($checker_exe>0) {
                mysqli_query($con, "INSERT INTO `cart`(`employee_id`,`item_id`,`quantity`,`location`,`branch_no`,`type`) VALUES ('$page_validator','$cart_item_id','1','$location','$branch','$cart_item_type')");
            } else {
                echo "<script>alert('We are at stock limit of $total_stock_count');</script>";
            }
        } else {
            echo "<script>alert('Sorry Stock is only limited in  $checker_exe');</script>";
        }
    } else {
        if ($total_stock_count>=$countity) {
            if ($checker_exe>=$countity) {
                mysqli_query($con, "INSERT INTO `cart`(`employee_id`,`item_id`,`quantity`,`location`,`branch_no`,`type`) VALUES ('$page_validator','$cart_item_id','$countity','$location','$branch','$cart_item_type')");
            } else {
                echo "<script>alert('Sorry Stock is updated due other person reserve the item in cart');</script>";
            }
        } else {
            echo "<script>alert('Sorry Stock is only limited in  $checker_exe');</script>";
        }
    }
}
if (isset($_POST['minus_cart'])) {
    $countity = $_POST['countity'];
    $cart_item_id = $_POST['cart_item_id'];
    $cart_item_type = $_POST['cart_item_type'];

    if ($cart_item_type==0) {
       $cart_list_check = mysqli_query($con, "SELECT SUM(quantity) AS value_sum FROM product_sold WHERE item_id ='$cart_item_id' AND location = '$location' AND branch = '$branch' ");
    } else {
        $cart_list_check = mysqli_query($con, "SELECT SUM(quantity) AS value_sum FROM item_sold WHERE item_id ='$cart_item_id' AND location = '$location' AND branch = '$branch' ");
    }
    $cart_list_check_row = mysqli_fetch_assoc($cart_list_check);
    $cart_quantity_check = $cart_list_check_row['value_sum'];
    $cart_quantity_check;
    ////////////////////////////////////////////////
    if ($cart_item_type==0) {
       $cart_list_check2 = mysqli_query($con, "SELECT SUM(quantity) AS value_sum2 FROM product_stock WHERE item_id ='$cart_item_id' AND location = '$location'  AND branch = '$branch' ");
    } else {
        $cart_list_check2 = mysqli_query($con, "SELECT SUM(quantity) AS value_sum2 FROM item_stock WHERE item_id ='$cart_item_id' AND location = '$location'  AND branch = '$branch' ");
    }
    $cart_list_check_row2 = mysqli_fetch_assoc($cart_list_check2);
    $cart_quantity_check2 = $cart_list_check_row2['value_sum2'];
    ///////////////////////////////////////////////
    $cart_list = mysqli_query($con, "SELECT SUM(quantity) AS value_sum3 FROM cart WHERE item_id ='$cart_item_id' AND location = '$location' AND branch_no = '$branch' AND type = '$cart_item_type' ");
    $cart_list_row = mysqli_fetch_assoc($cart_list);
    $cart_quantity = $cart_list_row['value_sum3'];

    $total_stock_count = $cart_quantity_check2-$cart_quantity_check;
    $checker_exe = $total_stock_count-$cart_quantity;

    if ($countity=="") {
         if ($total_stock_count>=$countity) {
            if ($checker_exe>=$countity) {
                if ($checker_exe==$total_stock_count) {
                   echo "<script>alert('Quantity is below zero');</script>";
                } else {
                    mysqli_query($con, "INSERT INTO `cart`(`employee_id`,`item_id`,`quantity`,`location`,`branch_no`,`type`) VALUES ('$page_validator','$cart_item_id','-1','$location','$branch','$cart_item_type')");
                }
            } else {
                echo "<script>alert('Sorry Stock is updated due other person reserve the item in cart');</script>";
            }
        } else {
            echo "<script>alert('Sorry Stock is only limited in  $checker_exe');</script>";
        }
    } else {
        if ($total_stock_count>=$countity) {
            if ($checker_exe>=0) {
                mysqli_query($con, "INSERT INTO `cart`(`employee_id`,`item_id`,`quantity`,`location`,`branch_no`,`type`) VALUES ('$page_validator','$cart_item_id','-$countity','$location','$branch','$cart_item_type')");
            } else {
                echo "<script>alert('Sorry Stock is updated due other person reserve the $countity item in cart2 $checker_exe');</script>";
            }
        } else {
            echo "<script>alert('The quntity in your cart is $total_stock_count');</script>";
        }
    }
}
if (isset($_POST['cancel_cart'])) {
    $countity = $_POST['countity'];
    $cart_item_id = $_POST['cart_item_id'];
    $cart_item_type = $_POST['cart_item_type'];
    $cart_quantity = $_POST['cart_quantity'];

    if ($cart_item_type==0) {
       $cart_list_check = mysqli_query($con, "SELECT SUM(quantity) AS value_sum FROM product_sold WHERE item_id ='$cart_item_id' AND location = '$location' AND branch = '$branch' ");
    } else {
        $cart_list_check = mysqli_query($con, "SELECT SUM(quantity) AS value_sum FROM item_sold WHERE item_id ='$cart_item_id' AND location = '$location' AND branch = '$branch' ");
    }
    $cart_list_check_row = mysqli_fetch_assoc($cart_list_check);
    $cart_quantity_check = $cart_list_check_row['value_sum'];
    $cart_quantity_check;
    ////////////////////////////////////////////////
    if ($cart_item_type==0) {
       $cart_list_check2 = mysqli_query($con, "SELECT SUM(quantity) AS value_sum2 FROM product_stock WHERE item_id ='$cart_item_id' AND location = '$location'  AND branch = '$branch' ");
    } else {
        $cart_list_check2 = mysqli_query($con, "SELECT SUM(quantity) AS value_sum2 FROM item_stock WHERE item_id ='$cart_item_id' AND location = '$location'  AND branch = '$branch' ");
    }
    $cart_list_check_row2 = mysqli_fetch_assoc($cart_list_check2);
    $cart_quantity_check2 = $cart_list_check_row2['value_sum2'];
    ///////////////////////////////////////////////
    $cart_list = mysqli_query($con, "SELECT SUM(quantity) AS value_sum3 FROM cart WHERE item_id ='$cart_item_id' AND location = '$location' AND branch_no = '$branch' AND type = '$cart_item_type' ");
    $cart_list_row = mysqli_fetch_assoc($cart_list);
    $cart_quantity = $cart_list_row['value_sum3'];

    $total_stock_count = $cart_quantity_check2-$cart_quantity_check;
    $checker_exe = $total_stock_count-$cart_quantity;

    if ($countity=="") {
         if ($total_stock_count>=$countity) {
            if ($checker_exe>=$countity) {
                if ($checker_exe==$total_stock_count) {
                   echo "<script>alert('Quantity is below zero');</script>";
                } else {
                    mysqli_query($con, "INSERT INTO `cart`(`employee_id`,`item_id`,`quantity`,`location`,`branch_no`,`type`) VALUES ('$page_validator','$cart_item_id','-$cart_quantity','$location','$branch','$cart_item_type')");
                }
            } else {
                echo "<script>alert('Sorry Stock is updated due other person reserve the item in cart');</script>";
            }
        } else {
            echo "<script>alert('Sorry Stock is only limited in  $checker_exe');</script>";
        }
    } else {
        if ($total_stock_count>=$countity) {
            if ($checker_exe>=$countity) {
                mysqli_query($con, "INSERT INTO `cart`(`employee_id`,`item_id`,`quantity`,`location`,`branch_no`,`type`) VALUES ('$page_validator','$cart_item_id','-$countity','$location','$branch','$cart_item_type')");
            } else {
                echo "<script>alert('Sorry Stock is updated due other person reserve the item in cart');</script>";
            }
        } else {
            echo "<script>alert('Sorry Stock is only limited in  $checker_exe');</script>";
        }
    }
}
if(isset($_SESSION['valid']))
    {
        ////////important per page
     
        ////////
        ?>
        <style type="text/css"> #nav6,#nav-5-1 {color: white; font-weight: bold; background-color:gray;} #nav-5-1{padding: 8px; border-radius: 2px; position: static;} #nav6:hover,#nav-5-1:hover { color: white; background-color: black; text-shadow: none;}</style>
        <?php
        /////////
     	if ($getloc=="") {
     		$header_info = "";
     	} else {
     		$header_info = strtoupper($getloc);
     	}
		?>
		<div class="row">
        <div class="col-sm"></div>
        <div class="col-sm-11" style="margin: auto;">
        <div id="add_stock"  style="background-color: white; padding: 25px; border-radius: 5px;">
        <label class="mb-1"><strong>SHOPPING CART</strong></label>
        <div style="text-align: right;">
        <form method="post">
            <button type="submit" name="cart_ui" formaction="dashboard.php?dash=nav23" style="background-color: white; border-radius: 5em; border-color: white; box-shadow: ; width: 50px; margin: auto; "><i class="fa fa-check" aria-hidden="true" style="font-size: 30px;"></i></button>
            <button type="submit" name="refresher"  style="background-color: white; border-radius: 5em; border-color: white; box-shadow: ; width: 50px; margin: auto; "><i class="fa fa-arrow-left" aria-hidden="true" style="font-size: 30px;"></i></button>
        </form>
        </div>
        <br>
        <div class="row">
        <div class="col-sm-12">
            <!-------------------------------------------------------->
            <strong><header style="position: relative; font-weight: bold; color: gray; padding-top: 2px;">ITEM</header></strong>
            <br>
            <div class="table-responsive" style=" border:white; border-radius: 5px; padding: 2px;">
                <table class="table table-responsive-sm-8 mb-0">
                    <thead>
                        <tr style="text-align: left;">
                            <th><strong>NAME</strong></th>
                            <th><strong>QUANTITY</strong></th>
                            <th><strong>PRICE</strong></th>
                            <th><strong>TYPE</strong></th>
                            <th><strong>TOTAL PRICE</strong></th>
                        </tr>
                    </thead>
                    <?php
                        $cart_list_sql = mysqli_query($con, "SELECT DISTINCT item_id, type FROM cart WHERE employee_id = '$page_validator' ORDER BY type");
                        while ($cart_list_sql_row = mysqli_fetch_assoc($cart_list_sql)) {
                            $cart_item_id = $cart_list_sql_row['item_id'];
                            $cart_type = $cart_list_sql_row['type'];

                            if ($cart_type==0) {
                                $product_fetch = mysqli_query($con, "SELECT * FROM products WHERE id = '$cart_item_id' ");
                                $product_fetch_row = mysqli_fetch_assoc($product_fetch);
                                $item_name = $product_fetch_row['product_name'];
                                $item_price = $product_fetch_row['price'];

                                $cart_list = mysqli_query($con, "SELECT SUM(quantity) AS value_sum3 FROM cart WHERE item_id ='$cart_item_id' AND location = '$location' AND branch_no = '$branch' AND type = 0 ");
                                $cart_list_row = mysqli_fetch_assoc($cart_list);
                                $cart_quantity = $cart_list_row['value_sum3'];
                                $type_type = "Product";

                            } else {
                                $product_fetch = mysqli_query($con, "SELECT * FROM equipments WHERE id = '$cart_item_id' ");
                                $product_fetch_row = mysqli_fetch_assoc($product_fetch);
                                $item_name = $product_fetch_row['equip_name'];
                                $item_price = $product_fetch_row['price'];

                                $cart_list = mysqli_query($con, "SELECT SUM(quantity) AS value_sum3 FROM cart WHERE item_id ='$cart_item_id' AND location = '$location' AND branch_no = '$branch' AND type = 1 ");
                                $cart_list_row = mysqli_fetch_assoc($cart_list);
                                $cart_quantity = $cart_list_row['value_sum3'];
                                $type_type = "Equipment";
                            }
                            $total_price_price = $cart_quantity*$item_price;
                            $total_total += $total_price_price;
                            if ($cart_quantity==0) {
                                # code...
                            } else {
                                ?>
                                <form id="" method="POST">
                                    <tr style="text-align: left;">
                                        <td><?php echo ucfirst($item_name);?></td>
                                        <td><?php echo ucfirst($cart_quantity);?></td>
                                        <td>₱ <?php echo ucfirst($item_price);?></td>
                                        <td><?php echo ucfirst($type_type);?></td>
                                        <td>₱ <?php echo ucfirst($total_price_price);?> </td>
                                        <input type="hidden" name="cart_item_id" value="<?php echo($cart_item_id)?>">
                                        <input type="hidden" name="cart_item_type" value="<?php echo($cart_type)?>">
                                        <input type="hidden" name="cart_item_quantity" value="<?php echo($cart_quantity)?>">
                                        <td><input type="" name="countity" style="width: 40px; border-radius: 5px; border-color: 1px solid gray; text-align: center;" pattern="[0-9]{0,5}"></td>
                                        <td><button type="submit" name="minus_cart"  style="background-color: white; border-radius: 5em; border-color: white; box-shadow: ; width: 30px; margin: auto; "><i class="fa fa-minus" aria-hidden="true" style="font-size: 12px;"></i></button></td>
                                        <td><button type="submit" name="add_cart"  style="background-color: white; border-radius: 5em; border-color: white; box-shadow: ; width: 30px; margin: auto; "><i class="fa fa-plus" aria-hidden="true" style="font-size: 12px;"></i></button></td>
                                        <td><button type="submit" name="cancel_cart"  style="background-color: white; border-radius: 5em; border-color: white; box-shadow: ; width: 30px; margin: auto; "><i class="fa fa-trash" aria-hidden="true" style="font-size: 12px;"></i></button></td>
                                    </tr>
                                </form>
                            <?php
                            }           
                        }
                        $taxable_amount = $total_total/1.12;
                        $percentage = 12;
                        $vat_amount = ($percentage / 100) * $taxable_amount;
                        if ($total_total==0) {
                                echo "<script>alert('Cart is empty');</script>";
                                ?><meta http-equiv="refresh" content="0;url=dashboard.php?dash=nav6" /><?php
                            }
                        ?>
                            <tr style="text-align: left;">
                                <td><strong>VAT: </strong></td>
                                <td><strong>TAXABLE AMOUNT: </strong></td>
                                <td><strong>TAX VAT: </strong></td>
                                <td></td>
                                <td><strong>TOTAL: </strong></td>
                                <td><input type="hidden" name="cart_item_id" value="<?php echo($product_id)?>"></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style="text-align: left;">
                                <td> 12% </td>
                                <td>₱ <?php echo round($taxable_amount)?></td>
                                <td>₱ <?php echo round($vat_amount)?></td>
                                <td></td>
                                <td style="text-decoration-line: underline; text-decoration-style: double;">₱ <?php echo ucfirst($total_total)?></td>
                                <td><input type="hidden" name="cart_item_id" value="<?php echo($product_id)?>"></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>

                        <?php
                        //echo "string",$total_total;
                    ?>
                </table>
            </div>
            <!-------------------------------------------------------->
        </div>

        <div class="col-sm"></div>
        </div>
        <br>
        </div>
        </div>
        <div class="col-sm"></div>
        </div>
		<?php
    }
else
    {
        header("location: index.php");
    }
?>
</body>
</html>