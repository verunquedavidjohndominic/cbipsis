<?php session_start();
error_reporting(1);
date_default_timezone_set('Asia/Manila');
include("connection.php");
$date = ucfirst(date('F j, Y'));

$page_validator = $_SESSION['valid'];
$class_checker = mysqli_query($con, "SELECT * FROM login WHERE id = '$page_validator'") or die("error logging in");
$row = mysqli_fetch_assoc($class_checker);
$class = $row['class'];
$location = $row['location'];
$branch = $row['branch'];
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<?php
$bn_list = mysqli_query($con, "SELECT * FROM branch");
$bn_no = mysqli_query($con, "SELECT * FROM branch_no");
if (isset($_POST['submit'])) {

    $username = strtolower($_POST['username']);
    $password = md5($_POST['password']);
    $class_pick = $_POST['class_pick'];
    $email = strtolower($_POST['email']);
    $firstname = strtolower($_POST['firstname']);
    $lastname = strtolower($_POST['lastname']);
    $middlename = strtolower($_POST['middlename']);
    $nameext = strtolower($_POST['nameext']);
    $location = $_POST['location'];
    $branch_no = $_POST['branch_no'];
    $act_creator = $_SESSION['valid'];

    mysqli_query($con, "INSERT INTO `login`(`username`, `password`, `class`, `email`, `name`, `surename`, `ml`, `ext`, `location`, `branch`, `act_creator`) VALUES ('$username','$password','$class_pick','$email','$firstname','$lastname','$middlename','$nameext','$location','$branch_no','$act_creator')");

     echo "<script>alert('Add user successfuly');</script>";

}

if(isset($_SESSION['valid']))
    {
        ////////important per page
        if ($class==0) {
           ?>
                <meta http-equiv="refresh" content="0;url=dashboard.php?dash=nav5" />
           <?php
        }
        ////////
        ?>
        <style type="text/css"> #nav1,#nav1-1 {color: white; font-weight: bold; background-color:gray;} #nav1-1{padding: 8px; border-radius: 2px; position: static;} #nav1:hover,#nav1-1:hover { color: white; background-color: black; text-shadow: none;}</style>
<div class="row">
<div class="col-sm"></div>
<div class="col-sm-11">
<form id="add_stock" method="POST" style="background-color: white; padding: 25px; border-radius: 5px;">
<label class="mb-1"><strong>ADD USER ACCOUNT</strong></label>
<br>
<br>
<div class="row">
<div class="col-sm"></div>
<div class="col-sm-5">
    <div class="form-group">
        <label class="mb-1"><strong>Class</strong></label>
        <?php
        if ($class==2) {
           ?>
            <select name = "class_pick" class="form-control">
                <option value="1">BRANCH MANAGER</option>
                <option value="0">CASHIER</option>
            </select>
           <?php
        } else {
            ?>
            <select name = "class_pick" class="form-control">
                <option value="0">CASHIER</option>
            </select>
           <?php
        }
        ?>
    </div>
    <div class="form-group">
        <label class="mb-1"><strong>Location</strong></label>
        <select name = "location" class="form-control">
            <?php
                if ($class==2) {
                     while($branch_row = mysqli_fetch_array($bn_list)):;
                    ?>
                        <option value="<?php echo ucfirst($branch_row[1]);?>">
                        <?php echo ucfirst($branch_row[1]);?>
                         </option>
                    <?php 
                        endwhile;
                } else {
                    ?>
                        <option value="<?php echo $location?>"><?php echo $location?></option>
                    <?php
                }
            ?>
        </select>
    </div>
    <div class="form-group">
        <label class="mb-1"><strong>Branch Number</strong></label>
        <select name = "branch_no" class="form-control">
            <?php
                while($branch_no_row = mysqli_fetch_array($bn_no)):;
                ?>
                    <option value="<?php echo ucfirst($branch_no_row[1]);?>">
                    <?php echo ucfirst($branch_no_row[1]);?>
                     </option>
                <?php 
                    endwhile;
            ?>
        </select>
    </div>
    <div class="form-group">
        <label class="mb-1"><strong>Username</strong></label>
        <input type="text" class="form-control" name="username" placeholder="Enter Username" required="">
    </div>
    <div class="form-group">
        <label class="mb-1"><strong>Password</strong></label>
        <input type="text" class="form-control" name="password" placeholder="Enter Password" required="">
    </div>
</div>
<div class="col-sm-5">
    <div class="form-group">
        <label class="mb-1"><strong>First Name</strong></label>
        <input type="text" class="form-control" name="firstname" placeholder="First Name" required="">
    </div>
    <div class="form-group">
        <label class="mb-1"><strong>MI</strong></label>
        <input type="text" class="form-control" name="middlename" placeholder="Middle Name" required="">
    </div>
    <div class="form-group">
        <label class="mb-1"><strong>Last Name</strong></label>
        <input type="text" class="form-control" name="lastname" placeholder="Last Name" required="">
    </div>
    <div class="form-group">
        <label class="mb-1"><strong>Extention</strong></label>
        <input type="text" class="form-control" name="nameext" placeholder="Name Extention">
    </div>
    <div class="form-group">
        <label class="mb-1"><strong>Email Address</strong></label>
        <input type="email" class="form-control" name="email" placeholder="Email Address" required="">
    </div>
</div>
<div class="col-sm"></div>
</div>
<br>
<div class="form-group">
    <button type="submit" id="submit" name="submit" class="btn btn-primary btn-block" style="background-color: #787312; border-color: #a1990b; box-shadow: 3px 3px 8px #b1b1b1, -3px -3px 8px #ffffff; width: 150px; margin: auto; ">Confirm</button>
</div>
</form>
</div>
<div class="col-sm"></div>
</div>
<?php
    }
else
    {
        header("location: index.php");
    }
?>
</body>
</html>