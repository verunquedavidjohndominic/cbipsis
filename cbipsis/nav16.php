<?php session_start();
error_reporting(1);
date_default_timezone_set('Asia/Manila');
include("connection.php");
$date = ucfirst(date('F j, Y'));

$page_validator = $_SESSION['valid'];
$class_checker = mysqli_query($con, "SELECT * FROM login WHERE id = '$page_validator'") or die("error logging in");
$row = mysqli_fetch_assoc($class_checker);
$class = $row['class'];
$location = $row['location'];
$branch = $row['branch'];

?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<?php


if(isset($_SESSION['valid']))
    {
        ////////important per page
        if ($class==0) {
           ?>
                <meta http-equiv="refresh" content="0;url=dashboard.php?dash=nav5" />
           <?php
        }
        ////////
        ?>
        <style type="text/css"> #nav3,#nav-3-3 {color: white; font-weight: bold; background-color:gray;} #nav-3-3{padding: 8px; border-radius: 2px; position: static;} #nav3:hover,#nav-3-3:hover { color: white; background-color: black; text-shadow: none;}</style>
        <?php
if (isset($_POST['info'])) {
    $info_id = $_POST['id'];
    ?>
        <meta http-equiv="refresh" content="0;url=dashboard.php?dash=nav18&id=<?php echo $info_id?>" />
    <?php
}
?>
<div class="row">
    <div class="col-sm"></div>
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <form name="search" method="post">
                            <header style="position: absolute; font-weight: bold; color: gray; padding-top: 2px;">STOCKS</header>
                            <div style="text-align: right;">
                                <input type="text" name="search" placeholder="Search Name" style="text-align: center; border-radius: 5em; border: 1px solid gray;">
                                <button type="submit" name="searchbnt_user" style="background-color: white; border-radius: 5cm; width: 25px; border: 1px solid gray;"><i class="fa fa-search"></i></button>
                            </div>
                        </form>
                        <br>
                        <br>
                        <?php
                        //////////////////////////////////////////////////////////////
                        $products = mysqli_query($con, "SELECT * FROM equipments ORDER BY equip_name");
                        $products_result_num_row = mysqli_num_rows($products);
                        if ($products_result_num_row<=0) {
                            ?>
                            <div class="table-responsive" style="max-height: 400px;">
                                <table class="table table-responsive-sm-6 mb-0">
                                    <thead>
                                        <tr style="text-align: center;">
                                            <th><strong>Name</strong></th>
                                            <th><strong>Description</strong></th>
                                            <th><strong>Price</strong></th>
                                        </tr>
                                    </thead>
                                    <?php
                          
                                
                                ?>
                                </table>
                                <br>
                                No Data
                            </div>
                            <?php
                        }
                        ?>
                        <?php
                        //////////////////////////////////////////////////////////////
                        ?>
                        <div id = "tago">
                        <div class="table-responsive" style="max-height: 400px;">
                            <table class="table table-responsive-sm-6 mb-0">
                                <thead>
                                    <tr style="text-align: center;">
                                        <th><strong>Name</strong></th>
                                        <th><strong>Description</strong></th>
                                        <th><strong>Price</strong></th>
                                    </tr>
                                </thead>
                                <?php
                                    $products = mysqli_query($con, "SELECT * FROM equipments ORDER BY equip_name");
                                    $products_result_num_row = mysqli_num_rows($products);
                                    if ($products_result_num_row>0) {
                                        while ($products_row = mysqli_fetch_assoc($products)) {
                                            $id = $products_row['id'];
                                            $product_name = $products_row['equip_name'];
                                            $description = $products_row['description'];
                                            $price = $products_row['price'];

                                            $category_fetch = mysqli_query($con, "SELECT * FROM category WHERE id = '$category'");
                                            $category_fetch_row = mysqli_fetch_assoc($category_fetch);
                                            $category_name = $category_fetch_row['category_name'];


                                                ?>
                                                <form id="" method="POST">
                                                    <input type="hidden" name="id" value="<?php echo $id?>">
                                                    <tr style="text-align: center;">
                                                        <td><?php echo ucfirst($product_name);?></td>
                                                        <td><?php echo ucfirst($description);?></td>
                                                        <td><?php echo ucfirst($price);?></td>
                                                        <td><div class="form-group"><button type="submit" id="submit" name="info" style="border: none;"><i class="fa fa-info-circle" style="color: black; background-color: white; padding-top: 15px; font-size: 20px"></i></button></div></td>
                                                    </tr>
                                                </form>
                                                <?php
                                        }
                                    } else {
                                        ?>
                                            <tr>
                                                <td>
                                                    <style type="text/css">#tago{display: none;}</style>
                                                    <p style="color:gray;"><?php echo $search?> is not exist</p>
                                                </td>
                                            </tr>
                                        <?php
                                    }
                            
                            ?>
                            </table>
                        </div>
                        </div>
                        <?php
                        if (isset($_POST['search'])) {
                            $search = $_POST['search'];
                        ?>
                            <style type="text/css">#tago{display: none;}</style>
                            <div class="table-responsive" style="max-height: 400px;">
                            <table class="table table-responsive-sm-6 mb-0">
                                <thead>
                                    <tr style="text-align: center;">
                                        <th><strong>Name</strong></th>
                                        <th><strong>Description</strong></th>
                                        <th><strong>Price</strong></th>
                                    </tr>
                                </thead>
                                <?php
                                    $products = mysqli_query($con, "SELECT * FROM equipments WHERE equip_name LIKE '%$search%' ORDER BY equip_name");
                                    $products_result_num_row = mysqli_num_rows($products);
                                    if ($products_result_num_row>0) {
                                        while ($products_row = mysqli_fetch_assoc($products)) {
                                            $id = $products_row['id'];
                                            $product_name = $products_row['equip_name'];
                                            $description = $products_row['description'];
                                            $price = $products_row['price'];

                                            $category_fetch = mysqli_query($con, "SELECT * FROM category WHERE id = '$category'");
                                            $category_fetch_row = mysqli_fetch_assoc($category_fetch);
                                            $category_name = $category_fetch_row['category_name'];


                                                ?>
                                                <form id="" method="POST">
                                                    <input type="hidden" name="id" value="<?php echo $id?>">
                                                    <tr style="text-align: center;">
                                                        <td><?php echo ucfirst($product_name);?></td>
                                                        <td><?php echo ucfirst($description);?></td>
                                                        <td><?php echo ucfirst($price);?></td>
                                                        <td><div class="form-group"><button type="submit" id="submit" name="info" style="border: none;"><i class="fa fa-info-circle" style="color: black; background-color: white; padding-top: 15px; font-size: 20px"></i></button></div></td>
                                                    </tr>
                                                </form>
                                                <?php
                                        }
                                    } else {
                                        ?>
                                            <tr>
                                                <td>
                                                    <style type="text/css">#tago{display: none;}</style>
                                                    <p style="color:gray;"><?php echo $search?> is not exist</p>
                                                </td>
                                            </tr>
                                        <?php
                                    }
                            
                            ?>
                            </table>
                        </div>
                        <?php
                        }
                        ?>
                        <!---->
                    </div>
                </div>
            </div>
            <br>
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <header style="position: absolute; font-weight: bold; color: gray; padding-top: 2px;">DAILY RECENT RECIEVED STOCK</header>
                        <br>
                        <br>
                        <br>
                        <div class="table-responsive" style="max-height: 400px;">
                            <table class="table table-responsive-sm-6 mb-0">
                                <thead>
                                    <tr style="text-align: center;">
                                        <th><strong>Name</strong></th>
                                        <th><strong>Quantity</strong></th>
                                        <th><strong>location</strong></th>
                                        <th><strong>Branch</strong></th>
                                        <th><strong>Reciever</strong></th>
                                    </tr>
                                </thead>
                                <?php
                                    $product_stock = mysqli_query($con, "SELECT * FROM item_stock WHERE `date` = '$date' ORDER BY id desc LIMIT 10");
                                    $product_stock_result_num_row = mysqli_num_rows($product_stock);
                                    if ($product_stock_result_num_row>0) {
                                        while ($product_stock_row = mysqli_fetch_assoc($product_stock)) {
                                            $pid = $product_stock_row['id'];
                                            $quantity = $product_stock_row['quantity'];
                                            $pdate = $product_stock_row['date'];
                                            $plocation = $product_stock_row['location'];
                                            $pbranch = $product_stock_row['branch'];
                                            $pitem_id = $product_stock_row['item_id'];
                                            $employee = $product_stock_row['employee'];
                                            $pending = $product_stock_row['pending'];

                                            $category_fetch = mysqli_query($con, "SELECT * FROM equipments WHERE id = '$pitem_id'");
                                            $category_fetch_row = mysqli_fetch_assoc($category_fetch);
                                            $category_name = $category_fetch_row['equip_name'];

                                            $employee_ayee = mysqli_query($con, "SELECT * FROM login WHERE id = '$employee'");
                                            $employee_ayee_row = mysqli_fetch_assoc($employee_ayee);
                                            $empname = $employee_ayee_row['name'];
                                            $empln = $employee_ayee_row['surename'];
                                                ?>
                                                <form id="" method="POST">
                                                    <input type="hidden" name="id" value="<?php echo $id?>">
                                                    <tr style="text-align: center;">
                                                        <td><?php echo ucfirst($category_name);?></td>
                                                        <td><?php echo ucfirst($quantity);?></td>
                                                        <td><?php echo ucfirst($plocation);?></td>
                                                        <td><?php echo ucfirst($pbranch);?></td>
                                                        <td><?php echo ucfirst($empname);?></td>
                                                    </tr>
                                                </form>
                                                <?php
                                        }
                                    } else {
                                        ?>
                                            <tr>
                                                <td>
                                                    <p style="color:gray;">All branch did not recieve any stock today</p>
                                                </td>
                                            </tr>
                                        <?php
                                    }
                            
                            ?>
                            </table>
                        </div>
                        <!---->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm">
    </div>
    <div class="col-sm"></div>
</div>
<?php
    }
else
    {
        header("location: index.php");
    }
?>
</body>
</html>