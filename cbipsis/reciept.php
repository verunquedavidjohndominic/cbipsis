<?php
include('connection.php');
require('fpdf/fpdf.php');
$pdf = new FPDF();
$date = ucfirst(date('F j, Y h:m:s'));
$reciept_no = $_GET['reciept'];

$fetcher = mysqli_query($con, "SELECT * FROM transaction WHERE sold_no = '$reciept_no'");
$fetcher_row = mysqli_fetch_assoc($fetcher);
$page_validator = $fetcher_row['employee_id'];

$class_checker = mysqli_query($con, "SELECT * FROM login WHERE id = '$page_validator'") or die("error logging in");
$row = mysqli_fetch_assoc($class_checker);
$location = $row['location'];
$branch = $row['branch'];
$name = $row['name'];
$surename = $row['surename'];

$pdf->AddPage();
$pdf->SetFont('Arial','B',15);
$pdf->Cell(190,1,'JAMES HATCHERY',0,1,'C');
$pdf->Ln();
$pdf->SetFont('Arial','B',10);
$pdf->Cell(190,10,'Official Receipt',0,1,'C');
$pdf->Ln();
$pdf->SetFont('Arial','B',6);
$pdf->Cell(39,5,'',0,0,'L');
$pdf->Cell(12,5,'Receipt no:',0,0,'L');
$pdf->Cell(10,5,$reciept_no,0,0,'L');
$pdf->Ln();
$pdf->Ln();
$pdf->SetFont('Arial','B',6);
$pdf->Cell(40,5,'',0,0,'L');
$pdf->Cell(10,5,'Quantity',0,0,'L');
$pdf->Cell(40,5,'Item',0,0,'L');
$pdf->Cell(20,5,'Price',0,0,'L');
$pdf->Cell(20,5,'Type',0,0,'L');
$pdf->Cell(20,5,'Total Price',0,0,'L');

$pprice_sum1 = mysqli_query($con, "SELECT * FROM product_sold WHERE sold_no = '$reciept_no'");
$pprice_sum_num_row1 = mysqli_num_rows($pprice_sum1);
$iprice_sum1 = mysqli_query($con, "SELECT * FROM item_sold WHERE sold_no = '$reciept_no' ");
$iprice_sum_num_row1 = mysqli_num_rows($iprice_sum1);

$product_sold_sql = mysqli_query($con, "SELECT * FROM product_sold WHERE sold_no = '$reciept_no' ");
while ($product_sold_row = mysqli_fetch_assoc($product_sold_sql)) {
	$pquantity = $product_sold_row['quantity'];
	$ptotal_rec = $product_sold_row['total_rec'];
	$pitem_id = $product_sold_row['item_id'];

	$item_info = mysqli_query($con, "SELECT * FROM products WHERE id = '$pitem_id'");
	$item_info_row = mysqli_fetch_assoc($item_info);
	$item_name = $item_info_row['product_name'];
	$item_price = $item_info_row['price'];

	$pprice_sum = mysqli_query($con, "SELECT SUM(total_rec) AS value_sum FROM product_sold WHERE sold_no = '$reciept_no'");
	$pprice_sum_row = mysqli_fetch_assoc($pprice_sum);
    $pprice_sum_total = $pprice_sum_row['value_sum'];

	$pdf->Ln();
	$pdf->SetFont('Arial','',6);
	$pdf->Cell(40,5,'',0,0,'L');
	$pdf->Cell(10,5,$pquantity,0,0,'L');
	$pdf->Cell(40,5,$item_name,0,0,'L');
	$pdf->Cell(20,5,$item_price,0,0,'L');
	$pdf->Cell(20,5,'Product',0,0,'L');
	$pdf->Cell(20,5,$ptotal_rec,0,0,'L');
}

$item_sold_sql = mysqli_query($con, "SELECT * FROM item_sold WHERE sold_no = '$reciept_no' ");
while ($item_sold_row = mysqli_fetch_assoc($item_sold_sql)) {
	$iquantity = $item_sold_row['quantity'];
	$itotal_rec = $item_sold_row['total_rec'];
	$iitem_id = $item_sold_row['item_id'];

	$item_info = mysqli_query($con, "SELECT * FROM equipments WHERE id = '$iitem_id'");
	$item_info_row = mysqli_fetch_assoc($item_info);
	$item_name = $item_info_row['equip_name'];
	$item_price = $item_info_row['price'];

	$iprice_sum = mysqli_query($con, "SELECT SUM(total_rec) AS value_sum FROM item_sold WHERE sold_no = '$reciept_no' ");
	$iprice_sum_num_row = mysqli_num_rows($iprice_sum);
	$iprice_sum_row = mysqli_fetch_assoc($iprice_sum);
    $iprice_sum_total = $iprice_sum_row['value_sum'];

	$pdf->Ln();
	$pdf->SetFont('Arial','',6);
	$pdf->Cell(40,5,'',0,0,'L');
	$pdf->Cell(10,5,$iquantity,0,0,'L');
	$pdf->Cell(40,5,$item_name,0,0,'L');
	$pdf->Cell(20,5,$item_price,0,0,'L');
	$pdf->Cell(20,5,'Equipment',0,0,'L');
	$pdf->Cell(20,5,$itotal_rec,0,0,'L');
}

if ($pprice_sum_num_row1<=0) {
	$total_price_rec1 = 0;
} else {
	$total_price_rec1 = $pprice_sum_total;
}
if ($iprice_sum_num_row1<=0) {
	$total_price_rec2 = 0;
} else {
	$total_price_rec2 = $iprice_sum_total;
} 


$total_price_rec = $total_price_rec2+$total_price_rec1;

$taxable_vat = round($total_price_rec/1.12);
$vat = round((12 / 100) * $taxable_vat);

$cash_receive = mysqli_query($con, "SELECT * FROM costumer WHERE sold_no='$reciept_no' ");
$cash_receive_row = mysqli_fetch_assoc($cash_receive);
$cash_receive_show = $cash_receive_row['cash_rec'];

$change_coins = $cash_receive_show - $total_price_rec;

$pdf->Ln();
$pdf->Ln();
$pdf->SetFont('Arial','B',6);
$pdf->Cell(40,5,'',0,0,'L');
$pdf->Cell(10,5,'Vat: 12%',0,0,'L');
$pdf->Cell(2,5,'',0,0,'L');
$pdf->Cell(18,5,'Taxable Amount:',0,0,'L');
$pdf->Cell(20,5,$taxable_vat,0,0,'L');
$pdf->Cell(10,5,'Tax Vat:',0,0,'L');
$pdf->Cell(17,5,$vat,0,0,'L');
$pdf->Cell(13,5,'Total Price:',0,0,'L');
$pdf->Cell(30,5,$total_price_rec,0,0,'L');
$pdf->Ln();
$pdf->SetFont('Arial','B',6);
$pdf->Cell(40,5,'',0,0,'L');
$pdf->Cell(10,5,'Cash Receive:',0,0,'L');
$pdf->Cell(2,5,'',0,0,'L');
$pdf->Cell(18,5,'',0,0,'L');
$pdf->Cell(20,5,'',0,0,'L');
$pdf->Cell(10,5,'',0,0,'L');
$pdf->Cell(17,5,'',0,0,'L');
$pdf->Cell(13,5,'',0,0,'L');
$pdf->Cell(30,5,$cash_receive_show,0,0,'L');
$pdf->Ln();
$pdf->SetFont('Arial','B',6);
$pdf->Cell(40,5,'',0,0,'L');
$pdf->Cell(10,5,'Cash Change:',0,0,'L');
$pdf->Cell(2,5,'',0,0,'L');
$pdf->Cell(18,5,'',0,0,'L');
$pdf->Cell(20,5,'',0,0,'L');
$pdf->Cell(10,5,'',0,0,'L');
$pdf->Cell(17,5,'',0,0,'L');
$pdf->Cell(13,5,'',0,0,'L');
$pdf->Cell(30,5,$change_coins,0,0,'L');
$pdf->Ln();
$pdf->Ln();
$pdf->SetFont('Arial','B',6);
$pdf->Cell(40,5,'',0,0,'L');
$pdf->Cell(15,5,'Prepared By:',0,0,'L');
$pdf->Cell(35,5,$name.' '.$surename,0,0,'L');
$pdf->Cell(10,5,'Location:',0,0,'L');
$pdf->Cell(25,5,$location,0,0,'L');
$pdf->Cell(10,5,'Branch:',0,0,'L');
$pdf->Cell(20,5,$branch,0,0,'L');
$pdf->Ln();
$pdf->SetFont('Arial','',6);
$pdf->Cell(40,5,'',0,0,'L');
$pdf->Cell(15,5,'Record Time:',0,0,'L');
$pdf->Cell(35,5,$date,0,0,'L');

$pdf->Output();
?>