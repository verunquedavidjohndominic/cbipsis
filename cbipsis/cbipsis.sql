-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 17, 2022 at 12:57 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cbipsis`
--

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE `branch` (
  `id` int(225) NOT NULL,
  `branch_name` varchar(225) NOT NULL,
  `activity` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`id`, `branch_name`, `activity`) VALUES
(1, 'Tacloban City', '0');

-- --------------------------------------------------------

--
-- Table structure for table `branch_no`
--

CREATE TABLE `branch_no` (
  `id` int(225) NOT NULL,
  `branch_no` int(225) NOT NULL,
  `activity` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branch_no`
--

INSERT INTO `branch_no` (`id`, `branch_no`, `activity`) VALUES
(1, 1, '0');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(225) NOT NULL,
  `employee_id` int(225) NOT NULL,
  `item_id` int(225) NOT NULL,
  `quantity` int(225) NOT NULL,
  `type` int(11) NOT NULL,
  `location` varchar(225) NOT NULL,
  `branch_no` int(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `category_name` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `costumer`
--

CREATE TABLE `costumer` (
  `id` int(225) NOT NULL,
  `name` varchar(225) NOT NULL,
  `surename` varchar(225) NOT NULL,
  `ml` varchar(225) NOT NULL,
  `extention` varchar(225) NOT NULL,
  `contact` varchar(225) NOT NULL,
  `employee_id` int(225) NOT NULL,
  `cash_rec` int(225) NOT NULL,
  `sold_no` varchar(225) NOT NULL,
  `address` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `equipments`
--

CREATE TABLE `equipments` (
  `id` int(225) NOT NULL,
  `equip_name` varchar(225) NOT NULL,
  `description` varchar(225) NOT NULL,
  `price` int(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `farmtool`
--

CREATE TABLE `farmtool` (
  `id` int(225) NOT NULL,
  `name` varchar(225) NOT NULL,
  `description` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `farmtool_stock`
--

CREATE TABLE `farmtool_stock` (
  `id` int(225) NOT NULL,
  `quantity` int(225) NOT NULL,
  `tool_id` int(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item_sold`
--

CREATE TABLE `item_sold` (
  `id` int(225) NOT NULL,
  `quantity` int(225) NOT NULL,
  `date` varchar(225) NOT NULL,
  `location` varchar(225) NOT NULL,
  `branch` int(225) NOT NULL,
  `item_id` int(225) NOT NULL,
  `employee` int(225) NOT NULL,
  `pending` int(225) NOT NULL,
  `sold_no` varchar(225) NOT NULL,
  `total_rec` varchar(225) NOT NULL,
  `remarks` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item_stock`
--

CREATE TABLE `item_stock` (
  `id` int(225) NOT NULL,
  `quantity` int(225) NOT NULL,
  `date` varchar(225) NOT NULL,
  `location` varchar(225) NOT NULL,
  `branch` int(225) NOT NULL,
  `item_id` int(225) NOT NULL,
  `employee` int(225) NOT NULL,
  `pending` int(225) NOT NULL,
  `del_inbox` int(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(225) NOT NULL,
  `username` varchar(225) NOT NULL,
  `password` varchar(225) NOT NULL,
  `class` int(225) NOT NULL,
  `email` varchar(225) NOT NULL,
  `name` varchar(225) NOT NULL,
  `surename` varchar(225) NOT NULL,
  `ml` varchar(225) NOT NULL,
  `ext` varchar(225) DEFAULT NULL,
  `location` varchar(225) NOT NULL,
  `branch` int(225) NOT NULL,
  `act_creator` int(225) NOT NULL,
  `blocked` int(225) NOT NULL,
  `logout` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `username`, `password`, `class`, `email`, `name`, `surename`, `ml`, `ext`, `location`, `branch`, `act_creator`, `blocked`, `logout`) VALUES
(20, 'admin', '21232f297a57a5a743894a0e4a801fc3', 2, 'cbipsis2022@gmail.com', 'owner', '', '', NULL, 'Tacloban City', 1, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(225) NOT NULL,
  `product_name` varchar(225) NOT NULL,
  `category` int(225) NOT NULL,
  `description` varchar(225) NOT NULL,
  `price` int(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_sold`
--

CREATE TABLE `product_sold` (
  `id` int(225) NOT NULL,
  `quantity` int(225) NOT NULL,
  `date` varchar(225) NOT NULL,
  `location` varchar(225) NOT NULL,
  `branch` int(225) NOT NULL,
  `item_id` int(225) NOT NULL,
  `employee` int(225) NOT NULL,
  `pending` int(225) NOT NULL,
  `sold_no` varchar(225) NOT NULL,
  `total_rec` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_stock`
--

CREATE TABLE `product_stock` (
  `id` int(225) NOT NULL,
  `quantity` int(225) NOT NULL,
  `date` varchar(225) NOT NULL,
  `location` varchar(225) NOT NULL,
  `branch` int(225) NOT NULL,
  `item_id` int(225) NOT NULL,
  `employee` int(225) NOT NULL,
  `pending` int(225) NOT NULL,
  `del_inbox` int(225) NOT NULL,
  `remarks` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE `transaction` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `date` varchar(11) NOT NULL,
  `sold_no` varchar(225) NOT NULL,
  `pending` int(225) NOT NULL,
  `del_inbox` int(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `used_tool`
--

CREATE TABLE `used_tool` (
  `id` int(225) NOT NULL,
  `tool_id` int(225) NOT NULL,
  `condition` varchar(225) NOT NULL,
  `incharge` varchar(225) NOT NULL,
  `quantity` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `branch`
--
ALTER TABLE `branch`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `branch_name` (`branch_name`);

--
-- Indexes for table `branch_no`
--
ALTER TABLE `branch_no`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `branch_no` (`branch_no`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`),
  ADD KEY `location` (`location`),
  ADD KEY `branch_no` (`branch_no`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `category_name` (`category_name`);

--
-- Indexes for table `costumer`
--
ALTER TABLE `costumer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `equipments`
--
ALTER TABLE `equipments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `equip_name` (`equip_name`);

--
-- Indexes for table `farmtool`
--
ALTER TABLE `farmtool`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `farmtool_stock`
--
ALTER TABLE `farmtool_stock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_sold`
--
ALTER TABLE `item_sold`
  ADD PRIMARY KEY (`id`),
  ADD KEY `location` (`location`),
  ADD KEY `branch` (`branch`);

--
-- Indexes for table `item_stock`
--
ALTER TABLE `item_stock`
  ADD PRIMARY KEY (`id`),
  ADD KEY `location` (`location`),
  ADD KEY `branch` (`branch`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `location` (`location`),
  ADD KEY `branch` (`branch`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_name` (`product_name`);

--
-- Indexes for table `product_sold`
--
ALTER TABLE `product_sold`
  ADD PRIMARY KEY (`id`),
  ADD KEY `location` (`location`),
  ADD KEY `branch` (`branch`);

--
-- Indexes for table `product_stock`
--
ALTER TABLE `product_stock`
  ADD PRIMARY KEY (`id`),
  ADD KEY `location` (`location`),
  ADD KEY `branch` (`branch`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `used_tool`
--
ALTER TABLE `used_tool`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `branch`
--
ALTER TABLE `branch`
  MODIFY `id` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `branch_no`
--
ALTER TABLE `branch_no`
  MODIFY `id` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `costumer`
--
ALTER TABLE `costumer`
  MODIFY `id` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `equipments`
--
ALTER TABLE `equipments`
  MODIFY `id` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `farmtool`
--
ALTER TABLE `farmtool`
  MODIFY `id` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `farmtool_stock`
--
ALTER TABLE `farmtool_stock`
  MODIFY `id` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `item_sold`
--
ALTER TABLE `item_sold`
  MODIFY `id` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `item_stock`
--
ALTER TABLE `item_stock`
  MODIFY `id` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `product_sold`
--
ALTER TABLE `product_sold`
  MODIFY `id` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `product_stock`
--
ALTER TABLE `product_stock`
  MODIFY `id` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `used_tool`
--
ALTER TABLE `used_tool`
  MODIFY `id` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `cart_ibfk_1` FOREIGN KEY (`location`) REFERENCES `branch` (`branch_name`) ON UPDATE CASCADE,
  ADD CONSTRAINT `cart_ibfk_2` FOREIGN KEY (`branch_no`) REFERENCES `branch_no` (`branch_no`) ON UPDATE CASCADE;

--
-- Constraints for table `item_sold`
--
ALTER TABLE `item_sold`
  ADD CONSTRAINT `item_sold_ibfk_1` FOREIGN KEY (`location`) REFERENCES `branch` (`branch_name`) ON UPDATE CASCADE,
  ADD CONSTRAINT `item_sold_ibfk_2` FOREIGN KEY (`branch`) REFERENCES `branch_no` (`branch_no`) ON UPDATE CASCADE;

--
-- Constraints for table `item_stock`
--
ALTER TABLE `item_stock`
  ADD CONSTRAINT `item_stock_ibfk_1` FOREIGN KEY (`location`) REFERENCES `branch` (`branch_name`) ON UPDATE CASCADE,
  ADD CONSTRAINT `item_stock_ibfk_2` FOREIGN KEY (`branch`) REFERENCES `branch_no` (`branch_no`) ON UPDATE CASCADE;

--
-- Constraints for table `login`
--
ALTER TABLE `login`
  ADD CONSTRAINT `branch` FOREIGN KEY (`branch`) REFERENCES `branch_no` (`branch_no`) ON UPDATE CASCADE,
  ADD CONSTRAINT `location` FOREIGN KEY (`location`) REFERENCES `branch` (`branch_name`) ON UPDATE CASCADE;

--
-- Constraints for table `product_sold`
--
ALTER TABLE `product_sold`
  ADD CONSTRAINT `product_sold_ibfk_1` FOREIGN KEY (`location`) REFERENCES `branch` (`branch_name`) ON UPDATE CASCADE,
  ADD CONSTRAINT `product_sold_ibfk_2` FOREIGN KEY (`branch`) REFERENCES `branch_no` (`branch_no`) ON UPDATE CASCADE;

--
-- Constraints for table `product_stock`
--
ALTER TABLE `product_stock`
  ADD CONSTRAINT `product_stock_ibfk_1` FOREIGN KEY (`location`) REFERENCES `branch` (`branch_name`) ON UPDATE CASCADE,
  ADD CONSTRAINT `product_stock_ibfk_2` FOREIGN KEY (`branch`) REFERENCES `branch_no` (`branch_no`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
