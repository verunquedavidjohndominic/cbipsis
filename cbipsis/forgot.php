<?php
error_reporting(1);
include("connection.php");

use PHPMailer\PHPMailer\PHPMailer; 
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';

$mail = new PHPMailer(true);
?>

<!DOCTYPE html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cloud Based Interbranch Poultry Sales Information System</title>
    <link href="design/css/design.css" rel="stylesheet">
<title>Forgot Password</title>
</head>
<body>
<body class="h-100" data-typography="roboto" data-theme-version="light" data-layout="vertical" data-nav-headerbg="color_3" data-headerbg="color_1" data-sidebar-style="mini" data-sibebarbg="color_3" data-sidebar-position="fixed" data-header-position="fixed" data-container="wide" direction="ltr" data-primary="color_1" style="background-image: url('design/a.png') !important">
    <div class="authincation h-100 mt-5 pt-5">
        <div class="container h-100">
            <div class="row justify-content-center h-100 align-items-center">
                <div class="col-md-6">
                    <div class="authincation-content" style="box-shadow: 3px 3px 8px #b1b1b1, -3px -3px 8px #ffffff;">
                        <div class="row no-gutters">
                            <div class="col-xl-12">
                                <div class="auth-form">
                                    <h4 class="text-center mb-4">Forgot your password?</h4>
                                    <form id="login" action="" method="post">
                                        <div class="form-group">
                                            <label class="mb-1"><strong>Enter email</strong></label>
                                            <input id="text" type="email" name="email" class="form-control" style="margin-bottom: 20px;border-radius: 25px;box-shadow: inset 8px 8px 8px #cbced1, inset -8px -8px 8px #ffffff;" placeholder="Enter Email" required>
                                        </div>
                                        <?php
                                        if(isset($_POST['submit'])){
                                            $from = $_POST['email']; // this is the sender's Email address
                                                 
                                                 $emailuser = mysqli_query($con, "SELECT * FROM login WHERE email='$from'");
                                                 $check_row = mysqli_fetch_assoc($emailuser);
                                                 if (empty($check_row)) {
                                                     echo "<p style='color:red;'>Email address did not exist</p>";
                                                 } else {
                                                    $emailuser2 = mysqli_query($con, "SELECT * FROM login WHERE email='$from'");
                                                    $row = mysqli_fetch_assoc($emailuser2);
                                                    $email = $row['email'];
                                                    $passwordfrom = $row['password'];
                                                    $username = $row['username'];
                                                    $login_id = $row['id'];

                                                    if (!empty($row)) {
                                                        try {
                                                            $mail->isSMTP();                                    
                                                            $mail->Host = 'smtp.gmail.com';
                                                            $mail->SMTPAuth = true;                           
                                                            $mail->Username = 'cbipsis2022@gmail.com';           
                                                                    $mail->Password = 'cbipsis1234';                         
                                                            $mail->SMTPSecure = 'tls';                          
                                                            $mail->Port = 587;  

                                                            $mail->setFrom('cbipsis2022@gmail.com', "Cloud Based Interbranch Poultry Sales Information System");
                                                            $mail->addAddress($email, "Receiver");

                                                            $mail->Subject = 'Forgoten Password';
                                                             $mail->Body    = 'Please Click the link to change your password cbipsis2022.epizy.com/link_cp.php?access_token='.$passwordfrom.'&user_access_verification_number='.$login_id;

                                                            $mail->send();
                                                             echo "<p style='color:green;'>Please check your Email to proceed</p>";
                                                        } catch (Exception $e) {
                                                            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
                                                        }
                                                    }
                                                    else
                                                    {
                                                    echo "account doesnt registered";
                                                    }
                                                 }    
                                            }
                                        ?>
                                        <div class="text-center">
                                            <input id='submit' type="submit" name="submit" class="btn btn-primary btn-block"style="background-color: #787312; border-color: #a1990b; box-shadow: 3px 3px 8px #b1b1b1, -3px -3px 8px #ffffff; " value="Recover">
                                        </div>
                                    </form>
                                    <div class="new-account mt-3">
                                        <p>Already have an account? <a class="text-primary" href="index.php">Log in now</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="vendor/global/global.min.js"></script>
    <script src="vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="js/custom.min.js"></script>
    <script src="js/deznav-init.js"></script>
</body>
</html> 