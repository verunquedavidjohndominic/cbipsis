<?php session_start();
error_reporting(1);
date_default_timezone_set('Asia/Manila');
include("connection.php");
$date = ucfirst(date('F j, Y'));
$date2 = ucfirst(date('F j, Y h:m:s'));
$page_validator = $_SESSION['valid'];
$class_checker = mysqli_query($con, "SELECT * FROM login WHERE id = '$page_validator'") or die("error logging in");
$row = mysqli_fetch_assoc($class_checker);
$class = $row['class'];
$location = $row['location'];
$branch = $row['branch'];
$bn_list = mysqli_query($con, "SELECT * FROM branch");
$bn_no = mysqli_query($con, "SELECT * FROM branch_no");
$getid = $_GET['id'];
$getloc = $_GET['loc'];
$getbranch = $_GET['branch'];
$box_name_fetch = mysqli_query($con, "SELECT * FROM products WHERE id = '$getid'");
$box_name_row = mysqli_fetch_assoc($box_name_fetch);
$box_name = strtoupper($box_name_row['product_name']);
$sold_no_for_reciept = $_GET['sold_no'];
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<?php   
if(isset($_SESSION['valid']))
    {
        ////////important per page
     
        ////////
        ?>
        <style type="text/css"> #nav6,#nav-5-1 {color: white; font-weight: bold; background-color:gray;} #nav-5-1{padding: 8px; border-radius: 2px; position: static;} #nav6:hover,#nav-5-1:hover { color: white; background-color: black; text-shadow: none;}</style>
        <?php
        /////////
     	if ($getloc=="") {
     		$header_info = "";
     	} else {
     		$header_info = strtoupper($getloc);
     	}
		?>
		<div class="row">
        <div class="col-sm"></div>
        <div class="col-sm-11">
        <div id="add_stock"  style="background-color: white; padding: 25px; border-radius: 5px;">
        <div style="text-align: right;">
        <form method="post">
            <button type="submit" name="print" formaction="reciept.php?reciept=<?php echo $sold_no_for_reciept;?>" formtarget="_blank" style="background-color: white; border-radius: 5em; border-color: white; box-shadow: ; width: 50px; margin: auto; "><i class="fa fa-print" aria-hidden="true" style="font-size: 30px;"></i></button>
            <button type="submit" formaction="dashboard.php?dash=nav6"  style="background-color: white; border-radius: 5em; border-color: white; box-shadow: ; width: 50px; margin: auto; "><i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 30px;"></i></button>
        <br>
        <br>
        <br>
        <div class="row">
        <div class="col-sm-10" style="text-align: left; background-color: lightgray; margin: auto; color: black; border-radius: 5px;">
            <!-------------------------------------------------------->
            <br>
            <br>
            <strong><header style=" font-weight: bold; padding-top: 2px; text-align: center;">BIRDSHAVEN POULTRY STORE</header></strong>
            <strong><header style=" font-weight: bold; padding-top: 2px; text-align: center;">Official Reciept</header></strong>
            <br>
            <!-------------------------------------------------------->
            <div class="row">
                <div class="col-sm-8" style="margin: auto;">
                    <div class="table-responsive" style=" border:white; border-radius: 5px; padding: 2px;">
                        <table class="table table-responsive-sm-8 mb-0">
                            <thead>
                                <tr style="text-align: left; color: black;">
                                    <th><strong style="text-align: left; color: black; font-size: 10px;">QUANTITY</strong></th>
                                    <th><strong style="text-align: left; color: black; font-size: 10px;">ITEM</strong></th>
                                    <th><strong style="text-align: left; color: black; font-size: 10px;">PRICE</strong></th>
                                    <th><strong style="text-align: left; color: black; font-size: 10px;">TYPE</strong></th>
                                    <th><strong style="text-align: left; color: black; font-size: 10px;">TOTAL PRICE</strong></th>
                                </tr>
                            </thead>
                            <?php
                                $product_sold_sql = mysqli_query($con, "SELECT * FROM product_sold WHERE sold_no = '$sold_no_for_reciept' ");
                                while ($product_sold_row = mysqli_fetch_assoc($product_sold_sql)) {
                                    $pquantity = $product_sold_row['quantity'];
                                    $plocation = $product_sold_row['location'];
                                    $pbranch = $product_sold_row['branch'];
                                    $employee = $product_sold_row['employee'];
                                    $ptotal_rec = $product_sold_row['total_rec'];
                                    $pitem_id = $product_sold_row['item_id'];

                                    $item_info = mysqli_query($con, "SELECT * FROM products WHERE id = '$pitem_id'");
                                    $item_info_row = mysqli_fetch_assoc($item_info);
                                    $item_name = $item_info_row['product_name'];
                                    $item_price = $item_info_row['price'];

                                    $pprice_sum = mysqli_query($con, "SELECT SUM(total_rec) AS value_sum FROM product_sold WHERE sold_no = '$sold_no_for_reciept'");
                                    $pprice_sum_row = mysqli_fetch_assoc($pprice_sum);
                                    $pprice_sum_total = $pprice_sum_row['value_sum'];

                                    $item_price_price = $item_price*$pquantity;
                                    ?>
                                        <tr style="text-align: left; color: black; font-size: 12px;">
                                            <td><?php echo ucfirst($pquantity);?></td>
                                            <td><?php echo ucfirst($item_name);?></td>
                                            <td><?php echo ucfirst($item_price);?></td>
                                            <td>Product</td>
                                            <td>₱ <?php echo ucfirst($item_price_price);?> </td>
                                        </tr>
                                    <?php
                                }
                                $item_sold_sql = mysqli_query($con, "SELECT * FROM item_sold WHERE sold_no = '$sold_no_for_reciept' ");
                                while ($item_sold_row = mysqli_fetch_assoc($item_sold_sql)) {
                                    $iquantity = $item_sold_row['quantity'];
                                    $itotal_rec = $item_sold_row['total_rec'];
                                    $iitem_id = $item_sold_row['item_id'];

                                    $item_info = mysqli_query($con, "SELECT * FROM equipments WHERE id = '$iitem_id'");
                                    $item_info_row = mysqli_fetch_assoc($item_info);
                                    $item_name = $item_info_row['equip_name'];
                                    $item_price = $item_info_row['price'];

                                    $iprice_sum = mysqli_query($con, "SELECT SUM(total_rec) AS value_sum FROM item_sold WHERE sold_no = '$sold_no_for_reciept' ");
                                    $iprice_sum_row = mysqli_fetch_assoc($iprice_sum);
                                    $iprice_sum_total = $iprice_sum_row['value_sum'];

                                    $item_price_price = $item_price*$iquantity;
                                     ?>
                                        <tr style="text-align: left; color: black; font-size: 12px;">
                                            <td><?php echo ucfirst($pquantity);?></td>
                                            <td><?php echo ucfirst($item_name);?></td>
                                            <td><?php echo ucfirst($item_price);?></td>
                                            <td>Equipment</td>
                                            <td>₱ <?php echo ucfirst($item_price_price);?> </td>
                                        </tr>
                                    <?php

                                }

                                $cash_receive = mysqli_query($con, "SELECT * FROM costumer WHERE sold_no='$sold_no_for_reciept' ");
                                $cash_receive_row = mysqli_fetch_assoc($cash_receive);
                                $cash_receive_show = $cash_receive_row['cash_rec'];

                                $total_price_rec = $pprice_sum_total+$iprice_sum_total;
                                $taxable_vat = round($total_price_rec/1.12);
                                $vat = round((12 / 100) * $taxable_vat);

                                $change_coins = $cash_receive_show - $total_price_rec;
                            ?>
                            <tr style="text-align: left; color: black; font-size: 10px;">
                                <td><strong>VAT: </strong></td>
                                <td><strong>TAXABLE AMOUNT: </strong></td>
                                <td><strong>TAX VAT: </strong></td>
                                <td></td>
                                <td><strong>TOTAL: </strong></td>
                            </tr>
                            <tr style="text-align: left; color: black; font-size: 12px;">
                                <td> 12% </td>
                                <td>₱ <?php echo round($taxable_vat)?></td>
                                <td>₱ <?php echo round($vat)?></td>
                                <td></td>
                                <td style="text-decoration-line: underline; text-decoration-style: double;">₱ <?php echo ucfirst($total_price_rec)?></td>
                            </tr>
                            <tr style="text-align: left; color: black; font-size: 12px;">
                                <td>Cash Receive: </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td style="">₱ <?php echo ucfirst($cash_receive_show)?></td>
                            </tr>
                            <tr style="text-align: left; color: black; font-size: 12px;">
                                <td>Change: </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td style="">₱ <?php echo ucfirst($change_coins)?></td>
                            </tr>
                        </table>
                        <br>
                        <br>
                        <br>
                        <br>
                    </div>
                </div>
            </div>
            <!-------------------------------------------------------->

        </div>
        </div>
        </form>
        </div>
        <br>
        </div>
        </div>
        <div class="col-sm"></div>
        </div>
		<?php
    }
else
    {
        header("location: index.php");
    }
?>
<br>
<br>
</body>
</html>