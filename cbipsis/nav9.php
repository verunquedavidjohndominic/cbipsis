<?php session_start();
error_reporting(1);
date_default_timezone_set('Asia/Manila');
include("connection.php");
$date = ucfirst(date('F j, Y'));

$page_validator = $_SESSION['valid'];
$class_checker = mysqli_query($con, "SELECT * FROM login WHERE id = '$page_validator'") or die("error logging in");
$row = mysqli_fetch_assoc($class_checker);
$class = $row['class'];
$location = $row['location'];
$branch = $row['branch'];
$get_search = $_GET['search'];
$get_limit = $_GET['page'];
if (isset($_POST['search_btn'])) {
    $search = $_POST['search'];
    ?>
        <meta http-equiv="refresh" content="0;url=dashboard.php?dash=nav9&search=<?php echo($search)?>" />
    <?php
}
if (isset($_POST['limit'])) {
    $limiter = $get_limit+10;
    ?>
        <meta http-equiv="refresh" content="0;url=dashboard.php?dash=nav9&page=<?php echo($limiter)?>" />
    <?php
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<?php


if(isset($_SESSION['valid']))
    {
        ////////important per page
        if ($class==0) {
           ?>
                <meta http-equiv="refresh" content="0;url=dashboard.php?dash=nav5" />
           <?php
        }
        ////////
        ?>
        <div id="add_stock" class="col-sm-11" style="background-color: white; padding: 5px; border-top-right-radius:5px; border-top-left-radius:5px; margin: auto;">
            <form name="search" method="post" class="form-control" style="border: black;">
                <header style="position: absolute; font-weight: bold; color: gray; padding-top: 2px;">SOLD INBOX</header>
                <div style="text-align: right;">
                    <input type="text" name="search" placeholder="Search Receipt" style="text-align: center; border-radius: 5em; border: 1px solid black;">
                    <button type="submit" name="search_btn" style="background-color: white; border-radius: 5cm; width: 25px; border: 1px solid gray;"><i class="fa fa-search"></i></button>
                </div>
            </form>
            <div class="col-sm-12" style="margin: auto;">
                <div class="table-responsive" style=" border:white; border-radius: 5px; padding: 2px; max-height: 400px;">
                    <table class="table table-responsive-sm-8 mb-0">
                        <thead>
                            <tr style="text-align: left; color: black; position: sticky;">
                                <th><strong style="text-align: left; color: black; font-size: 10px;">Receipt number</strong></th>
                                <th><strong style="text-align: left; color: black; font-size: 10px;"></strong></th>
                                <th><strong style="text-align: left; color: black; font-size: 10px;">branch</strong></th>
                                <th><strong style="text-align: left; color: black; font-size: 10px;">location</strong></th>
                                <th><strong style="text-align: left; color: black; font-size: 10px;">Date</strong></th>
                                <th><strong style="text-align: left; color: black; font-size: 10px;"></strong></th>
                                <th><strong style="text-align: left; color: black; font-size: 10px;"><form><button type="submit" formaction="readall.php?sold_no=<?php echo $tsold_no;?>&del=<?php echo $tsold_no;?>" style="background-color: white; border-color: white; box-shadow:1px 1px gray ; width: 100px; margin: auto; text-align: center; border-radius: 5em;  font-size: 12px; ">Mark as read all</button>
                                        </form></strong></th>
                                <th><form><strong style="text-align: left; color: black; font-size: 10px;"><button type="submit" formaction="delsoldallinbox.php?sold_no=<?php echo $tsold_no;?>&del=<?php echo $tsold_no;?>" style="background-color: white; border-color: white; box-shadow:1px 1px gray ; width: 100px; margin: auto; text-align: center; border-radius: 5em;  font-size: 12px; ">Delete all</button>
                                        </form></strong></th>
                            </tr>
                        </thead>
                        <?php
                            if ($get_search=="") {
                                $where_search = "";
                            } else {
                                $where_search = "AND sold_no = '$get_search'";
                            }
                            if ($get_limit=="") {
                                $the_limit = "5";
                            } else {
                                $the_limit = $get_limit;
                            }
                            $transaction_sql = mysqli_query($con, "SELECT * FROM transaction WHERE del_inbox = '0' $where_search ORDER BY pending, id DESC LIMIT $the_limit ");
                            while ($transaction_sql_row = mysqli_fetch_assoc($transaction_sql)) {
                                $temployee = $transaction_sql_row['employee_id'];
                                $tdate = $transaction_sql_row['date'];
                                $tsold_no = $transaction_sql_row['sold_no'];
                                $tpending = $transaction_sql_row['pending'];
                                $tdelete = $transaction_sql_row['del_inbox'];

                                $location_fetched = mysqli_query($con, "SELECT * FROM login WHERE id = '$temployee' ");
                                $location_fetched_row = mysqli_fetch_assoc($location_fetched);
                                $lloc = $location_fetched_row['location'];
                                $lbranch = $location_fetched_row['branch'];

                                if ($tpending==0) {
                                    $text_show = "NEW";
                                } else {
                                    $text_show = "";
                                }
                                ?>
                                <tr style="text-align: left; color: black;">
                                    <td style="font-size: 10px; font-weight: bold;"><?php echo $tsold_no ?></strong></td>
                                    <td style="font-size: 10px; font-weight: bold;"></td>
                                    <td style="font-size: 10px; font-weight: bold;"><?php echo $lbranch ?></td>
                                    <td style="font-size: 10px; font-weight: bold;"><?php echo $lloc ?></td>
                                    <td style="font-size: 10px; font-weight: bold;"><?php echo $tdate ?></td>
                                    <td style="font-size: 10px; font-weight: bold;"><?php echo $text_show ?></td>   
                                    <td style="font-size: 10px; font-weight: bold;"></td>
                                    <td style="font-size: 10px; font-weight: bold;"></td>
                                    <td style="font-size: 10px; font-weight: bold;"></td>
                                    <td style="font-size: 10px; font-weight: bold;"><form method="post">
                                        <button type="submit" formaction="dashboard.php?dash=nav28&sold_no=<?php echo $tsold_no;?>&read=<?php echo $tpending;?>" style="background-color: white; border-color: white; box-shadow:1px 1px gray ; width: 30px; margin: auto; text-align: center; border-radius: 5em;  font-size: 12px; "><i class="fa fa-book" aria-hidden="true"></i></button>
                                        </form>
                                    </strong></th>
                                    <th><strong style="text-align: left; color: black; font-size: 10px;"><form method="post">
                                        <button type="submit" formaction="delsoldinbox.php?sold_no=<?php echo $tsold_no;?>&del=<?php echo $tsold_no;?>" style="background-color: white; border-color: white; box-shadow:1px 1px gray ; width: 30px; margin: auto; text-align: center; border-radius: 5em;  font-size: 12px; "><i class="fa fa-trash" aria-hidden="true"></i></button>
                                        </form>
                                    </strong></th>
                                </tr>
                                <?php
                            }
                        ?>
                    </table>
                    <br>
                    <?php
                        $costumer_sql_sold_no_num_rows2 = mysqli_num_rows($transaction_sql);
                        if ($costumer_sql_sold_no_num_rows2>0) {
                            if ($get_search=="") {
                                ?>
                                    <form method="POST" style="text-align: right;">
                                       <button id="hover" type="submit" name="limit" style="background-color: white; border-radius: 5cm; width: 100px; border: 1px solid gray;">See more</button>
                                    </form>
                                    <style type="text/css">
                                        #hover:hover {
                                            background-color: black;
                                            color: green;
                                        }
                                    </style>
                                <?php
                            } else {
                                
                            }
                        } else {
                                if ($costumer_sql_sold_no_num_rows2==0) {
                                    ?>
                                        <tr>
                                            <td ><?php echo $get_search;?> No Current Data</td>
                                        </tr>
                                        <br>
                                        <br>
                                        <br>
                                    <?php
                                } else {

                                }
                        }
                    ?>
                </div>
            </div>
        </div>
        <?php
    }
else
    {
        header("location: index.php");
    }
?>
</body>
</html>