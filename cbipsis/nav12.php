<?php session_start();
error_reporting(1);
date_default_timezone_set('Asia/Manila');
include("connection.php");
$date = ucfirst(date('F j, Y'));

$page_validator = $_SESSION['valid'];
$class_checker = mysqli_query($con, "SELECT * FROM login WHERE id = '$page_validator'") or die("error logging in");
$row = mysqli_fetch_assoc($class_checker);
$class = $row['class'];
$location = $row['location'];
$branch = $row['branch'];
$category_list = mysqli_query($con, "SELECT * FROM category");
$category_list2 = mysqli_query($con, "SELECT * FROM category");
$price_list = mysqli_query($con, "SELECT * FROM products");
$prod_list = mysqli_query($con, "SELECT * FROM products");
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<?php

if (isset($_POST['add_prod'])) {
    $name = $_POST['name'];
    $category = $_POST['category'];
    $description = $_POST['description'];
    $price = $_POST['price'];

    mysqli_query($con, "INSERT INTO `products`(`product_name`, `category`, `description`, `price`) VALUES ('$name','$category','$description','$price')");
    echo "<script>alert('Add ".$name." as products')</script>";
    ?>
        <meta http-equiv="refresh" content="0;url=dashboard.php?dash=nav12" />
    <?php
}
if (isset($_POST['up_price'])) {
    $select_price = $_POST['price_select'];
    $price = $_POST['price'];
    $selector = mysqli_query($con, "SELECT * FROM products WHERE id = '$select_price'");
    $selector_row = mysqli_fetch_assoc($selector);
    $name = $selector_row['product_name'];
    mysqli_query($con, "UPDATE `products` SET `price`= '$price' WHERE id = '$select_price'");
    echo "<script>alert('Product ".$name." price is updated to ".$price."')</script>";
    ?>
        <meta http-equiv="refresh" content="0;url=dashboard.php?dash=nav12" />
    <?php
}
if (isset($_POST['up_prod'])) {
    $select_price = $_POST['price_select'];
    $prod = $_POST['prod'];
    $selector = mysqli_query($con, "SELECT * FROM products WHERE id = '$select_price'");
    $selector_row = mysqli_fetch_assoc($selector);
    $name = $selector_row['product_name'];
    mysqli_query($con, "UPDATE `products` SET `product_name`= '$prod' WHERE id = '$select_price'");
    echo "<script>alert('Product ".$name."name price is updated to ".$prod."')</script>";
    ?>
        <meta http-equiv="refresh" content="0;url=dashboard.php?dash=nav12" />
    <?php
}
if (isset($_POST['add_cat'])) {
    $name = $_POST['name'];

    mysqli_query($con, "INSERT INTO `category`(`category_name`) VALUES ('$name')");
    echo "<script>alert('Add ".$name." as category')</script>";
    ?>
        <meta http-equiv="refresh" content="0;url=dashboard.php?dash=nav12" />
    <?php
}
if (isset($_POST['del_cat'])) {
    $category = $_POST['category'];
    $cat_name = $_POST['cat_name'];

    mysqli_query($con, "UPDATE `category` SET `category_name`= '$cat_name' WHERE id = '$category'");
    echo "<script>alert('successfuly updated category name to ".$cat_name."')</script>";
    ?>
        <meta http-equiv="refresh" content="0;url=dashboard.php?dash=nav12" />
    <?php
}
if(isset($_SESSION['valid']))
    {
        ////////important per page
        if ($class==0) {
           ?>
                <meta http-equiv="refresh" content="0;url=dashboard.php?dash=nav5" />
           <?php
        }
        ////////
        ?>
<div class="col-sm-11" style="margin: auto; background-color: white; padding: 25px; border-radius: 5px;">
         <form id="" method="POST" style="background-color: white; padding: 25px; border-radius: 5px;">
            <div class="col-sm">
                <p>Reminders:<br>
                    /Add product category first to completely add products<br>
                    /Description (Male/Female/Whole/white/Brown)<br>
                /Category (0-12 weeks, 12-52 weeks, 1year+)</p>
                <label class="mb-1"><strong>ADD NEW PRODUCTS</strong></label>
                <br>
                <div class="row">
                    <div class="col-sm">
                        <label class="mb-1"><strong>Name</strong></label>
                        <input type="text" class="form-control" name="name" placeholder="Enter Product Name" required="" style="text-align: center;">
                        <br>
                    </div>
                    <div class="col-sm">
                        <label class="mb-1"><strong>Description</strong></label>
                        <input type="text" class="form-control" name="description" placeholder="Enter description" required="" style="text-align: center;">
                        <br>
                    </div>
                    <div class="col-sm">
                        <label class="mb-1"><strong>Category</strong></label>
                        <select name = "category" class="form-control">
                        <?php 
                           while($category_list_row = mysqli_fetch_array($category_list)):;
                        ?>
                            <option value="<?php echo $category_list_row[0];?>">
                            <?php echo ucfirst($category_list_row[1]);?>
                            </option>
                        <?php 
                            endwhile;
                        ?>
                        </select>
                        <br>
                    </div>
                    <div class="col-sm">
                        <label class="mb-1"><strong>Price</strong></label>
                        <input type="number" class="form-control" name="price" placeholder="Enter Price" required="" style="text-align: center;">
                        <br>
                    </div>
                    <div class="col-sm">
                        <div class="form-group">
                            <label class="mb-1"><strong>&nbsp</strong></label>
                            <button type="submit" id="submit" name="add_prod" class="btn btn-primary btn-block" style="background-color: hsla(21, 51%, 17%, 1); border-color: white; width: 150px; margin: auto; ">Save</button>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </form>  
        <form id="" method="POST" style="background-color: white; padding: 25px; border-radius: 5px;">
            <div class="col-sm-11">
                <label class="mb-1"><strong>ADD CATEGORY</strong></label>
                <div class="row">
                    <div class="col-sm">
                        <label class="mb-1"><strong>Name</strong></label>
                        <input type="text" class="form-control" name="name" placeholder="Enter Category Name" required="" style="text-align: center;">
                    </div>
                    <div class="col-sm">
                        <div class="form-group">
                        <label class="mb-1"><strong>&nbsp</strong></label>
                        <button type="submit" id="submit" name="add_cat" class="btn btn-primary btn-block" style="background-color: hsla(21, 51%, 17%, 1); width: 150px; margin: auto; ">Save</button>
                        </div>
                    </div>
                    <div class="col-sm">
                    </div>
                    <div class="col-sm">
                    </div>
                </div>
            </div>
        </form> 
    <form id="" method="POST" style="background-color: white; padding: 25px; border-radius: 5px;">
        <div class="col-sm">
            <label class="mb-1"><strong>UPDATE CATEGORY</strong></label>
            <div class="row">
                <div class="col-sm">
                    <label class="mb-1"><strong>Product Category</strong></label>
                    <select name = "category" class="form-control">
                    <?php 
                       while($category_list_row2 = mysqli_fetch_array($category_list2)):;
                    ?>
                        <option value="<?php echo $category_list_row2[0];?>">
                        <?php echo ucfirst($category_list_row2[1]);?>
                        </option>
                    <?php 
                        endwhile;
                    ?>
                    </select>
                </div>
                <div class="col-sm">
                    <label class="mb-1"><strong>New Category</strong></label>
                    <input type="text" class="form-control" name="cat_name" placeholder="Enter New Category" required="" style="text-align: center;">
                </div>
                <div class="col-sm">
                    <div class="form-group">
                    <label class="mb-1"><strong>&nbsp</strong></label>
                    <button type="submit" id="submit" name="del_cat" class="btn btn-primary btn-block" style="background-color:hsla(21, 51%, 17%, 1); border-color: white; width: 150px; margin: auto; ">Confirm</button>
                    </div>
                </div>
                <div class="col-sm">
                </div>
            </div>
        </div>
    </form>
    <form id="" method="POST" style="background-color: white; padding: 25px; border-radius: 5px;">
        <div class="col-sm">
            <label class="mb-1"><strong>UPDATE PRICE</strong></label>
            <div class="row">
                <div class="col-sm">
                    <label class="mb-1"><strong>Product Name</strong></label>
                    <select name = "price_select" class="form-control">
                    <?php 
                       while($category_list_row3 = mysqli_fetch_array($price_list)):;
                    ?>
                        <option value="<?php echo $category_list_row3[0];?>">
                        <?php echo ucfirst($category_list_row3[1]);?>
                        </option>
                    <?php 
                        endwhile;
                    ?>
                    </select>
                </div>
                <div class="col-sm">
                    <label class="mb-1"><strong>New Price</strong></label>
                    <input type="text" class="form-control" name="price" placeholder="Enter New Price" required="" style="text-align: center;">
                </div>
                <div class="col-sm">
                    <div class="form-group">
                    <label class="mb-1"><strong>&nbsp</strong></label>
                    <button type="submit" id="submit" name="up_price" class="btn btn-primary btn-block" style="background-color:hsla(21, 51%, 17%, 1); border-color: white; width: 150px; margin: auto; ">Confirm</button>
                    </div>
                </div>
                <div class="col-sm">
                </div>
            </div>
        </div>
    </form>
    <form id="" method="POST" style="background-color: white; padding: 25px; border-radius: 5px;">
        <div class="col-sm">
            <label class="mb-1"><strong>UPDATE PRODUCT NAME</strong></label>
            <div class="row">
                <div class="col-sm">
                    <label class="mb-1"><strong>Product Name</strong></label>
                    <select name = "price_select" class="form-control">
                    <?php 
                       while($category_list_row3 = mysqli_fetch_array($prod_list)):;
                    ?>
                        <option value="<?php echo $category_list_row3[0];?>">
                        <?php echo ucfirst($category_list_row3[1]);?>
                        </option>
                    <?php 
                        endwhile;
                    ?>
                    </select>
                </div>
                <div class="col-sm">
                    <label class="mb-1"><strong>New Name</strong></label>
                    <input type="text" class="form-control" name="prod" placeholder="Enter New Name" required="" style="text-align: center;">
                </div>
                <div class="col-sm">
                    <div class="form-group">
                    <label class="mb-1"><strong>&nbsp</strong></label>
                    <button type="submit" id="submit" name="up_prod" class="btn btn-primary btn-block" style="background-color:hsla(21, 51%, 17%, 1); border-color: white; width: 150px; margin: auto; ">Confirm</button>
                    </div>
                </div>
                <div class="col-sm">
                </div>
            </div>
        </div>
    </form>

</div>
<?php
    }
else
    {
        header("location: index.php");
    }
?>
</body>
</html>