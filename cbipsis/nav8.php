<?php session_start();
error_reporting(1);
date_default_timezone_set('Asia/Manila');
include("connection.php");
$date = ucfirst(date('F j, Y'));
$page_validator = $_SESSION['valid'];
$class_checker = mysqli_query($con, "SELECT * FROM login WHERE id = '$page_validator'") or die("error logging in");
$row = mysqli_fetch_assoc($class_checker);
$class = $row['class'];
$location = $row['location'];
$branch = $row['branch'];
$bn_list = mysqli_query($con, "SELECT * FROM branch");
$bn_no = mysqli_query($con, "SELECT * FROM branch_no");
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<?php
if(isset($_SESSION['valid']))
    {
        ////////important per page
        if ($class==0) {
           ?>
                <meta http-equiv="refresh" content="0;url=dashboard.php?dash=nav5" />
           <?php
        }
        ////////
        $getid = $_GET['id'];
        $get_info = mysqli_query($con, "SELECT * FROM `login` WHERE `id` = '$getid'");
        $getinfo_row = mysqli_fetch_assoc($get_info);
	    $username = $getinfo_row['username'];
	    $password = $getinfo_row['password'];
	    $class1 = $getinfo_row['class'];
	    $email = $getinfo_row['email'];
	    $name = $getinfo_row['name'];
	    $surename = $getinfo_row['surename'];
	    $ml = $getinfo_row['ml'];
	    $ext = $getinfo_row['ext'];
	    $location1 = $getinfo_row['location'];
	    $branch = $getinfo_row['branch'];
	    $act_creator = $getinfo_row['act_creator'];


	    if ($class1==1) {
	    	$nav = "#nav-2-1";
	    	$position = "BRANCH MANAGER";
	    } else {
	    	$nav = "#nav-2-2";
	    }
	    if ($class1==2){
	    	$position = "MAIN ADMIN";
	    }
	    if ($class==0) {
	    	$position = "CASHIER";
	    }
        ?>
        <style type="text/css"> #nav2,<?php echo $nav?> {color: white; font-weight: bold; background-color:gray;} <?php echo $nav?>{padding: 8px; border-radius: 2px; position: static;} #nav2:hover,<?php echo $nav?>:hover { color: white; background-color: black; text-shadow: none;}</style>
        <?php
        /////////
        if (isset($_POST['save'])) {
        	$class_pick = $_POST['class_pick'];
        	$location = $_POST['location'];
        	$branch_no = $_POST['branch_no'];
        	$password = $_POST['password'];
        	$password2 = $_POST['password2'];
        	$firstname = $_POST['firstname'];
        	$firstname2 = $_POST['firstname2'];
        	$middlename = $_POST['middlename'];
        	$middlename2 = $_POST['middlename2'];
        	$lastname = $_POST['lastname'];
        	$lastname2 = $_POST['lastname2'];
        	$nameext = $_POST['nameext'];
        	$nameext2 = $_POST['nameext2'];
        	$email = $_POST['email'];
        	$email2 = $_POST['email2'];
        	if ($firstname=="") {
        		$firstname = $firstname2;
        	}
        	if ($password=="") {
        		$password = $password2;
        	} else {
        		$password = md5($password);
        	}
        	if ($middlename=="") {
        		$middlename = $middlename2;
        	}
        	if ($lastname=="") {
        		$lastname = $lastname2;
        	}
        	if ($nameext=="") {
        		$nameext = $nameext2;
        	}
        	if ($email=="") {
        		$email = $email2;
        	}

        	mysqli_query($con, "UPDATE `login` SET `password`='$password',`class`='$class_pick',`email`='$email',`name`='$firstname',`surename`='$lastname',`ml`='$middlename',`ext`='$nameext',`location`='$location',`branch`='$branch_no',`act_creator`='$page_validator' WHERE id = '$getid'");
        	echo "<script>alert('Account has been updated')</script>";
        	?>
            	<meta http-equiv="refresh" content="0;url=dashboard.php?dash=nav8&id=<?php echo $getid?>" />
           <?php
        }
		?>
		<div class="row">
		<div class="col-sm"></div>
		<div class="col-sm-11">
		<form id="add_stock" method="POST" style="background-color: white; padding: 25px; border-radius: 5px;">
		<label class="mb-1"><strong>EDIT ACCOUNT</strong></label>
		<br>
		<br>
		<div class="row">
		<div class="col-sm"></div>
		<div class="col-sm-5">
		    <div class="form-group">
		        <label class="mb-1"><strong>Class</strong></label>
		        <select name = "class_pick" class="form-control">
		        <option value="<?php echo ucfirst($class1)?>"><?php echo ucfirst($position)," (Current)"?></option>
		        <?php
		        if ($class==2) {
		           ?>
		            <option value="1">BRANCH MANAGER</option>
		            <option value="0">CASHIER</option>
		           <?php
		        } else {
		            ?>
		            <option value="0">CASHIER</option>
		           <?php
		        }
		        ?>
		        </select>
		    </div>
		    <div class="form-group">
		        <label class="mb-1"><strong>Location</strong></label>
		        <select name = "location" class="form-control">
		        	<option value="<?php echo ucfirst($location)?>"><?php echo ucfirst($location)," (Current)"?></option>
            <?php
                if ($class==2) {
                     while($branch_row = mysqli_fetch_array($bn_list)):;
                    ?>
                        <option value="<?php echo ucfirst($branch_row[1]);?>">
                        <?php echo ucfirst($branch_row[1]);?>
                         </option>
                    <?php 
                        endwhile;
                } else {
                    ?>
                        <option value="<?php echo $location?>"><?php echo $location?></option>
                    <?php
                }
            ?>
        </select>
		    </div>
		    <div class="form-group">
		        <label class="mb-1"><strong>Branch Number</strong></label>
		        <select name = "branch_no" class="form-control">
		        	<option value="<?php echo ucfirst($branch)?>"><?php echo ucfirst($branch)," (Current)"?></option>
		            <?php
		                while($branch_no_row = mysqli_fetch_array($bn_no)):;
		                ?>
		                    <option value="<?php echo ucfirst($branch_no_row[1]);?>">
		                    <?php echo ucfirst($branch_no_row[1]);?>
		                     </option>
		                <?php 
		                    endwhile;
		            ?>
		        </select>
		    </div>
		    <div class="form-group">
		        <label class="mb-1"><strong>Username</strong></label>
		        <input type="text" class="form-control" name="username" value="<?php echo($username)?>" readonly="">
		    </div>
		    <div class="form-group">
		        <label class="mb-1"><strong>Password</strong></label>
		        <input type="text" class="form-control" name="password" placeholder="Enter New Password">
		        <input type="hidden" name="password2" value="<?php echo($password)?>">
		    </div>
		</div>
		<div class="col-sm-5">
		    <div class="form-group">
		        <label class="mb-1"><strong>First Name</strong></label>
		        <input type="text" class="form-control" name="firstname" placeholder="<?php echo(ucfirst($name))?>">
		        <input type="hidden" name="firstname2" value="<?php echo($name)?>">
		    </div>
		    <div class="form-group">
		        <label class="mb-1"><strong>MI</strong></label>
		        <input type="text" class="form-control" name="middlename" placeholder="<?php echo(ucfirst($ml))?>">
		        <input type="hidden" name="middlename2" value="<?php echo($ml)?>">
		    </div>
		    <div class="form-group">
		        <label class="mb-1"><strong>Last Name</strong></label>
		        <input type="text" class="form-control" name="lastname" placeholder="<?php echo(ucfirst($surename))?>">
		        <input type="hidden" name="lastname2" value="<?php echo($surename)?>">
		    </div>
		    <div class="form-group">
		        <label class="mb-1"><strong>Extention</strong></label>
		        <input type="text" class="form-control" name="nameext" placeholder="<?php echo(ucfirst($ext))?>">
		        <input type="hidden" name="nameext2" value="<?php echo($ext)?>">
		    </div>
		    <div class="form-group">
		        <label class="mb-1"><strong>Email Address</strong></label>
		        <input type="email" class="form-control" name="email" placeholder="<?php echo(ucfirst($email))?>">
		        <input type="hidden" name="email2" value="<?php echo($email)?>">
		    </div>
		</div>
		<div class="col-sm"></div>
		</div>
		<br>
		<div class="row">
		<div class="col-sm"></div>
		<div class="col-sm">
		<div class="form-group">
		    <button type="submit" id="submit" name="save" class="btn btn-primary btn-block" style="background-color: #787312; border-color: #a1990b; box-shadow: 3px 3px 8px #b1b1b1, -3px -3px 8px #ffffff; width: 150px; margin: auto; ">Save</button>
		</div>
		</div>
		<div class="col-sm">
		<div class="form-group">
		    <a type="submit" id="submit" name="cancel" class="btn btn-primary btn-block" style="background-color: #787312; border-color: #a1990b; box-shadow: 3px 3px 8px #b1b1b1, -3px -3px 8px #ffffff; width: 150px; margin: auto; " href = "javascript:history.back(1)">Cancel</a>
		</div>
		</div>
		<div class="col-sm"></div>
		</div>
		</form>
		</div>
		<div class="col-sm"></div>
		</div>
		<?php
    }
else
    {
        header("location: index.php");
    }
?>
</body>
</html>